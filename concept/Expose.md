---
title: "Exposé: Softwareentwicklungspraktikum Spielentwicklung mit Javascript, WS 19/20, Gruppe NN"
institute: Ludwig-Maximilians-Universität München
date: 24. November 2019
papersize: a4
documentclass: article
lang: de-DE
linestretch: 1.25
fontfamily: times
fontsize: 12pt
numbersections: true
---

# Spielbeschreibung
Dies ist das Konzept für ein **2D Top-Down Rennspiel**, welches sowohl im Multiplayer- als auch im Singleplayermodus gespielt werden kann.
Dabei treten pro Rennen maximal 4 Spieler gleichzeitig gegeneinander an. Jeder Spieler hat vor Rennbeginn die Möglichkeit einen Username festzulegen, ein Fahrzeug auszuwählen und ggf. einem bestimmten Rennen (Lobby) beizutreten, um beispielsweise gegen bestimmte Mitspieler anzutreten. 
Jedes Rennen findet auf einer der 3 bereitsgestellten Rennstrecken statt und dauert 3 Runden. Es gelten die klassischen Rennspielregeln: Wer als Erstes durch das Ziel fährt, gewinnt.


## Spielregeln
Während eines Rennens gibt es bestimmte Regeln:

1. Bei Kollision mit dem Fahrbahnrand, einem Hindernis oder einem anderen Spieler werden alle an der Kollision beteiligten Spieler ausgebremst
2. Auf der Strecke befinden sich sog. Powerups *(sh. unten)*, welche von jedem Spieler eingesammelt und eingelöst werden können. Hierbei ist zu beachten, dass keine neuen Powerups eingesammelt werden können, solange ein Spieler ein Powerup bereits in seinem Inventar hat (d.h. ein vorher eingesammeltes Powerup noch nicht verbraucht wurde).

## User Interface
Jeder Spieler sieht neben dem aktuellen Kartenausschnitt auch weitere Hilfselemente. Dazu gehören:

1. Minimap, mit einer Übersicht der gesamten Strecke und den Positionen der anderen Spieler
2. Geschwindigkeitsanzeige
3. Aktueller Ranglistenplatz des Spielers
 
Die Fahrbahn der Rennstrecke wird als graue Straße mit weißer gestrichelter Mittellinie dargestellt. Außerhalb des befahrbaren Bereichs werden automatisch Bäume, Sträucher, Häuser und weitere Szenerieelemente platziert. Jeder Spieler steuert ein Fahrzeug in einer anderen Farbe.

## Steuerung

Die Steuerung wird mit einem EventListener auf dem dem `keyDown` bzw `keyUp` Event umgesetzt. Dabei kann das Fahrzeug entweder über die Tasten `W`, `A`, `S` und `D`, oder über die Pfeiltasten gesteuert werden. Mit der Eingabe `W` bzw. `ArrowUp` soll das Fahrzeug beschleunigen, mit der Taste `A` oder `ArrowLeft` lenkt das Fahrzeug nach links bzw. rotiert nach links, mit der Taste `D` oder `ArrowRight` wird nach rechts gelenkt bzw. rotiert und mit der Taste `S` oder `ArrowDown` bremst das Fahrzeug bzw. fährt es rückwärts.
Das Fahrzeug bewegt sich dabei solange in eine Richtung, bis die Taste wieder losgelassen wird. 
Mit der `space` Taste kann jeder Spieler das Powerup im Inventar (sofern vorhanden) einlösen. Dies wird ebenfalls mit dem EventListener `keyUp` umgesetzt.

## Power Ups
Es gibt drei verschiedene Arten von PowerUps, die auf der Strecke verteilt liegen und dem Spieler beim Einsammeln eine bestimmte Fähigkeit oder einen Bonus verleihen.
Wurde ein PowerUp eingesammelt, wird nach einem zufälligen Zeitraum ein neues PowerUp der gleichen Art an einer zufälligen Stelle der Strecke platziert. Das eingesammelte Powerup erscheint, dann im Inventar des Spielers und kann dann zu einem beliebigen Zeitpunkt eingelöst werden.

Die drei Arten der PowerUps definieren sich wie folgt:  

1. **"Speed Boost"**: Dieses Powerup besitzt die Fähigkeit, das Fahrzeug für kurze Zeit erheblich zu beschleunigen. Dabei wird die maximale Geschwindigkeit erhöht und der Spieler hat die Möglichkeit, andere Spieler leichter zu überholen.
2. **"Slow down first place"**: Mit dem zweiten Powerup kann ein Spieler den Spieler auf dem ersten Platz kurzzeitig behindern und langsamer machen. Dies verhindert das weite Absetzen und die Uneinholbarkeit des ersten Spielers.
3. **"Hurdle"**: Das dritte Powerup legt nach Einsatz ein Hindernis auf der aktuellen Spielerposition ab. Fährt ein anderer Spieler nun über dieses Hindernis, wird dieser kurzzeitig abgebremst.

# Potentielle Probleme
Während eines Rennens kann es passieren, dass einer der Spieler seine Internetverbindung verliert. Ist dies der Fall wird nicht das komplette Rennen abgebrochen, sondern einfach nur dieser Spieler entfernt. 
Sollte es vorkommen, dass nur ein Spieler existiert, kann dieser im Singleplayermodus gegen einen computer-gesteuerten Bot fahren, welcher sich auf einer Optimalspur bewegt. Gibt es zu viele Spieler werden mehrere Rennen mit jeweils 4 Spielern gestartet.
Ein weiteres Problem ist die Steuerung bei der Rückwärtsbewegung, da hierbei alle Richtungen umgekehrt werden müssen.

# Architektur
Die Verwaltung des Rennspiels wird in zwei Komponenten aufgeteilt. Einerseits den lokalen Clients und andererseits einem zentralen Server. Im folgenden werden deren Aufgaben genauer erläutert.

## Client
Der Client übernimmt bestimmte Aufgaben, die zwar lokal ausgeführt werden, allerdings immer mit dem Server synchronisiert werden müssen, d.h. bei jedem Funktionsaufruf wird eine Nachricht an den Server gesendet. Der Client hat dabei folgende Aufgaben:

- Steuerungshandling der Fahrzeuge (d.h. reagiert auf Tasteneingaben), dabei muss aber die neue Position nach Steuerung mit dem Server synchronisiert werden
- Einlösen des "Speed Boost" (also Erhöhung der aktuellen Geschwindigkeit)
- Rendern des aktuellen Spielauschnitts inkl. Minimap, Geschwindigkeitsanzeige und Rangliste

## Server
Die Client-Server Kommunikation erfolgt in Echtzeit mittels Websockets *(eingesetzte Library: socket.io)*. Der Server hat folgende Aufgaben:
- Verwaltung aller Spiele, d.h. der Server startet, beendet die Spiele und teilt Spieler zu Rennen zu
- Speicherung und Rendern aller bereitsgestellten Rennstrecken sowie zufällige Auswahl einer Strecke für einen Spiel
- Verwaltung aller beteiligten Spieler (Speicherung aller Positionen, Kollisionshandling, Zuweisung der Spieler zu Rennen)
- Erzeugung neuer Powerups auf der Rennstrecke bzw. Ausblenden dieser, wenn diese eingesammelt wurden
- Ausführung der Powerup Aktionen (außer Speed-Boost), also z.B. Speicherung der Positionen der abgelegten Hindernisse

# UML-Klassendiagramm

Abbildung \ref{umlclasses} stellt den Aufbau des Systems und das Zusammenspiel der verschiedenen Komponenten als UML-Klassendiagramm dar. Aus Gründen der Übersichtlichkeit wird das System mit einem gewissen Abstraktionsniveau abgebildet. So wird z.B. auf eine genaue Modellierung der Websockets verzichtet, da hier eine bestehende Bibliothek (socket.io) zum Einsatz kommt.

![UML 2.0 Klassendiagramm\label{umlclasses}](./uml/classes.png){width=120%}

# Zeitplan und Milestones
Die Entwicklung des Rennspieles soll nach dem unten aufgeführten Zeitplan geschehen.

## 25.11. - 16.12.
- Grundlegende Steuerung
- Grundlegende UI Elemente (Geschwindigkeit, Rangliste)
- Map Rendering und Synchronisation
- Kollisionserkennung
- Powerups
- Basic KI (Single Player Bot)

## 16.12. - 13.01.
- Verbesserung der Physik
- Bewegung der Map Section mit Fahrzeugbewegung
- Multiplayer
- Spielen beitreten
- Eye Candy
- Weitere Verbesserung der KI

## 13.01. - 03.02.
- Fertigstellung der UI
- Implementierung der HUD
- Verbesserung der Grafik
- Lobbies
- Mehr Rennstrecken
- Polishing
