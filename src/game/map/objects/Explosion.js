const MapObject = require("./MapObject");
const Coordinates = require("../Coordinates");

/**
 * An explosion animation
 *
 * @augments MapObject
 *
 */
class Explosion extends MapObject {
  /**
   * @constructs Explosion
   * @param {CanvasRenderingContext2D} [context] - canvas 2d context to draw on
   * @param {Image} [texture] - texture
   */
  constructor(context, texture) {
    super(undefined, context, texture, 25, 25, 15, 15);
    this.collidedWithHurdle = false;
  }

  /**
   * Show explosion
   *
   * @param {Coordinates} position - The position where the explosion should happen
   *
   * @returns {void}
   */
  explodeAt(position) {
    this.position = position;
    this.collidedWithHurdle = true;
  }
}

module.exports = Explosion;
