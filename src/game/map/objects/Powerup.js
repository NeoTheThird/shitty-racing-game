const MapObject = require("./MapObject");
const Coordinates = require("../Coordinates");
const POWERUP_TYPES = require("./POWERUP_TYPES");

/**
 * A collectible Powerup (one of three types)
 *
 * @augments MapObject
 *
 * @property {boolean} enabled - Visibility of powerup
 * @property {string} type - Random type of the powerup
 */
class Powerup extends MapObject {
  /**
   * @constructs Powerup
   * @param {Coordinates} position - position on the map
   * @param {POWERUP_TYPES} [type] - Powerup type
   * @param {boolean} [enabled] - visibility of the powerup
   * @param {CanvasRenderingContext2D} [context] - canvas 2d context to draw on
   * @param {Image} [texture] - texture
   */
  constructor(
    position,
    type = Powerup.getRandomPowerupType(),
    enabled = true,
    context,
    texture
  ) {
    super(position, context, texture, 10, 10, 15, 15);
    this.type = type;
    this.enabled = enabled;
  }

  /**
   * Reactivate powerup with a new type and show on map again
   *
   * @param {POWERUP_TYPES} type - The new type
   *
   * @returns {void}
   */
  reactivate(type = Powerup.getRandomPowerupType()) {
    this.enabled = true;
    this.type = type;
  }

  /**
   * Get a random powerup type from the predefined types
   *
   * @returns {POWERUP_TYPES} type - Powerup type
   */
  static getRandomPowerupType() {
    const keys = Object.keys(POWERUP_TYPES);
    let randomIndex = Math.floor(Math.random() * keys.length);
    return POWERUP_TYPES[keys[randomIndex]];
  }
}

module.exports = Powerup;
