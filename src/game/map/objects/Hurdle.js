const MapObject = require("./MapObject");
const Coordinates = require("../Coordinates");

/**
 * A bomb placed by a player using a powerup
 *
 * @augments MapObject
 *
 * @property {string} placedBy - Reference on the Player ID
 */
class Hurdle extends MapObject {
  /**
   * @constructs Hurdle
   * @param {Coordinates} position - Position of hurdle
   * @param {string} placedBy - Reference on the Player ID
   * @param {CanvasRenderingContext2D} [context] - canvas 2d context to draw on
   * @param {Image} [texture] - texture
   */
  constructor(position, placedBy, context, texture) {
    super(position, context, texture, 10, 10, 15, 15);
    this.placedBy = placedBy;
  }
}

module.exports = Hurdle;
