const Coordinates = require("../Coordinates");

/**
 * A generic object on the map
 *
 * @property {Coordinates} position - position on the map
 */
class MapObject {
  /**
   * @constructs MapObject
   * @param {Coordinates} position - position on the map
   * @param {CanvasRenderingContext2D} [context] - canvas 2d context to draw on
   * @param {Image} [texture] - texture
   * @param {number} [width] - width of the object on the map
   * @param {number} [height] - height of the object on the map
   * @param {number} [spriteWidth] - sprite width of the object in the asset
   * @param {number} [spriteHeight] - sprite height of the object in the asset
   */
  constructor(
    position,
    context,
    texture,
    width,
    height,
    spriteWidth,
    spriteHeight
  ) {
    this.position = new Coordinates(position);
    this.context = context;
    this.texture = texture;
    this.width = width;
    this.height = height;
    this.spriteWidth = spriteWidth;
    this.spriteHeight = spriteHeight;
    this.animationSpeed = 7;
    this.currentAnimationStep = 0;
    this.spriteSize = 15;
  }

  /**
   * Change the Animation
   *
   * @param {number} frameCount - object movement
   *
   * @returns {void}
   */
  update(frameCount) {
    if (frameCount % this.animationSpeed === 0) {
      this.currentAnimationStep = (this.currentAnimationStep + 1) % 6;
    }
  }

  /**
   * Draw the object on the map
   *
   * @returns {void}
   */
  draw() {
    this.renderPosition = this.position;

    this.context.setTransform(
      this.renderPosition.angle_cos,
      this.renderPosition.angle_sin,
      -this.renderPosition.angle_sin,
      this.renderPosition.angle_cos,
      this.renderPosition.x,
      this.renderPosition.y
    );
    this.context.drawImage(
      this.texture,
      this.currentAnimationStep * this.spriteSize,
      0,
      this.spriteWidth,
      this.spriteHeight,
      -(this.width / 2),
      -(this.height / 2),
      this.width,
      this.height
    );
    this.context.setTransform(1, 0, 0, 1, 0, 0);
  }
}

module.exports = MapObject;
