const MapObject = require("./MapObject");
const Coordinates = require("../Coordinates");

/**
 * Finishline
 *
 * @augments MapObject
 */
class FinishLine extends MapObject {
  /**
   * @constructs FinishLine
   * @param {Coordinates} position - position on the map
   * @param {CanvasRenderingContext2D} [context] - canvas 2d context to draw on
   * @param {Image} [texture] - texture
   */
  constructor(position, context, texture) {
    super(position, context, texture, 75, 15, 90, 15);
  }
}

module.exports = FinishLine;
