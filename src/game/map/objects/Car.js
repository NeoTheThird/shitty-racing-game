const MapObject = require("./MapObject");
const Coordinates = require("../Coordinates");
const KeyStorage = require("../../../libs/KeyStorage");

/**
 * A car (or player) on the map
 *
 * @augments MapObject
 *
 * @property {number} VMAX - top speed
 * @property {number} speed - current speed
 * @property {number} mod - speed modulator, negative for backwards
 * @property {number} ROTATION_STEP - number of radians to rotate on steering
 * @property {number} CAR_HEIGHT - height of the hitbox
 * @property {number} CAR_WIDTH - width of the hitbox
 * @property {boolean} equippedPowerup - is a powerup equipped
 * @property {KeyStorage} keyStorage - pressed keys
 * @property {number} lastCheckpoint - the last checkpoint the car passed
 * @property {number} currentRound - the current round of the player
 * @property {boolean} collided - did the player collide
 * @property {number} lastCoordinateHitbox - index of the last hitbox the car passed (exact)
 * @property {number} rank - Current rank among all other players
 */
class Car extends MapObject {
  /**
   * @constructs Car
   * @param {string} id - Car ID
   * @param {Coordinates} position - Starting position of the player
   * @param {string} name - Username
   * @param {string} color - Color of player's car
   * @param {CanvasRenderingContext2D} [context] - 2d context of the canvas
   * @param {Image} [texture] - car sprite set
   */
  constructor(id, position, name, color, context, texture) {
    super(position, context, texture, 32, 17.5, 50, 27);
    this.id = id;
    this.position = new Coordinates(position);
    this.renderPosition = new Coordinates(position);
    this.name = name;
    this.color = color;
    this.keyStorage = new KeyStorage();
    this.VMAX = 5;
    this.speed = 0;
    this.boost = false;
    this.enabled = true;
    this.lastCheckpoint = 0;
    this.lastCoordinateHitbox = 0;
    this.currentRound = 0;
    this.collided = false;
    this.rank = -1;
    this.blockTimeout = false;
    this.boostTimeout = false;
    this.equippedPowerup = false;
    this.mod = 0;
    this.ROTATION_STEP = 0.04; // 2.29°
  }

  /**
   * Update car's position related data with new data
   *
   * @param {Car} props - New data the current car should be updated with
   *
   * @returns {void}
   */
  updateWith(props) {
    this.speed = props.speed;
    this.position = props.position;
    this.rank = props.rank;
  }

  /**
   * Return position relevant information
   *
   * @returns {{rank: number, position: Coordinates, speed: number}} - The relevant data
   */
  getPositionData() {
    return {
      position: this.position,
      speed: this.speed,
      rank: this.rank
    };
  }

  /**
   * Activate boost powerup
   *
   * @param {number} duration - duration of the boost
   *
   * @returns {void}
   */
  activateBoost(duration = 2000) {
    this.boost = true;
    if (this.boostTimeout) clearTimeout(this.boostTimeout);
    this.boostTimeout = setTimeout(() => {
      this.boostTimeout = false;
      this.boost = false;
    }, duration);
  }

  /**
   * Trigger a powerup from the inventory
   *
   * @returns {void}
   */
  triggerPowerup() {
    this.equippedPowerup = false;
  }

  /**
   * Increment the current round
   *
   * @returns {void}
   */
  nextRound() {
    this.currentRound++;
  }

  /**
   * Indicate whether the car completed the last round
   *
   * @returns {boolean} - Completed last round or not
   */
  isLastRoundOver() {
    return this.currentRound === 4;
  }

  /**
   * Slow down the first car in the ranking
   *
   * @returns {void}
   */
  slowFirst() {
    this.speed *= -0.3;
  }

  /**
   * Collision with another car
   *
   * @returns {void}
   */
  onCarCollision() {
    if (!this.collided) {
      this.collided = true;
      this.speed *= -0.5;
      setTimeout(() => {
        this.collided = false;
      }, 500);
    }
  }

  /**
   * Block steering of the car
   *
   * @param {number} [time=500] - duration, set to -1 for indefinite
   *
   * @returns {void}
   */
  blockSteering(time = 500) {
    this.enabled = false;
    if (this.blockTimeout) clearTimeout(this.blockTimeout);
    if (time !== -1)
      this.blockTimeout = setTimeout(() => this.unblockSteering(), time);
  }

  /**
   * Unblock steering of the car
   *
   * @returns {void}
   */
  unblockSteering() {
    if (this.blockTimeout) clearTimeout(this.blockTimeout);
    this.blockTimeout = false;
    this.enabled = true;
  }

  /**
   * Control of the car
   *
   * @returns {void}
   */
  control() {
    if (this.keyStorage.goForwards()) {
      this.mod = 1;
      this.speed += this.mod * 0.1;
    } else if (this.keyStorage.goBackwards()) {
      this.mod = -0.6;
      this.speed += this.mod * 0.1;
    } else {
      this.speed *= 0.99;
    }

    // don't break top speed
    if (this.speed > this.VMAX) {
      this.speed = this.VMAX;
    } else if (this.speed < this.VMAX * -0.6) {
      this.speed = this.VMAX * -0.6;
    }

    // Apply boost powerup
    if (this.boost && !this.keyStorage.goBackwards()) {
      this.speed = this.VMAX + 3;
      this.mod = 1;
    }

    this.direction = this.speed >= 0 ? 1 : -1;

    // left/right
    if (this.enabled) {
      if (this.keyStorage.goLeft()) {
        this.position.angle -= this.ROTATION_STEP * this.direction;
      } else if (this.keyStorage.goRight()) {
        this.position.angle += this.ROTATION_STEP * this.direction;
      }
    }

    this.position.angle_cos = Math.cos(this.position.angle);
    this.position.angle_sin = Math.sin(this.position.angle);

    this.position.x += this.speed * this.position.angle_cos;
    this.position.y += this.speed * this.position.angle_sin;
  }

  /**
   * Reset position of the player to last passed checkpoint
   *
   * @param {object[]} checkpoints - Coordinates of the checkpoints
   *
   * @returns {void}
   */
  resetPositionToLastCheckpoint(checkpoints) {
    this.position.x = checkpoints[this.lastCheckpoint].x;
    this.position.y = checkpoints[this.lastCheckpoint].y;
    this.position.angle = checkpoints[this.lastCheckpoint].angle;
    this.speed = 0;
  }
}

module.exports = Car;
