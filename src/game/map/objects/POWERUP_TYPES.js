/**
 * @enum POWERUP_TYPES
 * @description Enum for mapping powerup types  to strings
 */
const POWERUP_TYPES = {
  BOOST: "speedBoost",
  HURDLE: "hurdle",
  SLOW_FIRST: "slowFirst"
};

module.exports = POWERUP_TYPES;
