const Coordinates = require("./Coordinates");

/**
 * Collision detection
 */
class CollisionDetection {
  /**
   * Check if player is in a hitbox at a specified coordinate
   *
   * @param {object} object - The object for which the collision shall be checked
   * @param {Coordinates[]} coordinates - The coordinates of the objects where a collision can happen
   * @param {number} radius - Size of the hitbox around the coordinated
   * @param {boolean} [transform=false] - Whether the coordinates are already in the form [{x: number, y: number}]
   *
   * @returns {number} index - Index of the hitbox which was hit or -1 if no hitbox was hit
   */
  static detect(object, coordinates, radius, transform = false) {
    if (transform) coordinates = CollisionDetection.transform(coordinates);
    for (let i = 0; i < coordinates.length; i++) {
      const currentPos = coordinates[i];
      if (
        Math.pow(object.position.x - currentPos.x, 2) +
          Math.pow(object.position.y - currentPos.y, 2) <=
        Math.pow(radius, 2)
      ) {
        // Player is in at least one coordinate hitbox
        return i;
      }
    }
    // Player isn't in ANY hitbox
    return -1;
  }

  static transform(array) {
    return array.map(entry => {
      return entry.position;
    });
  }
}

module.exports = CollisionDetection;
