const CoordinateCalculation = require("./CoordinateCalculation");
const FinishLine = require("./objects/FinishLine");
const Collision = require("./CollisionDetection");
const maps = require("../../assets/maps");

/**
 * A race track
 *
 * @property {object[]} points - Coordinate points (incl. interpolated)
 * @property {ImageData} mapImage - Prerendered static map
 * @property {ImageData} minimapImage - Prerendered static minimap
 * @property {number} STREET_WIDTH - Width of the road
 */
class Map {
  /**
   * @constructs Map
   * @param {Map} model - Map model
   * @param {CanvasRenderingContext2D} [context] - map rendering context
   * @param {CanvasRenderingContext2D} [minimapContext] - minimap rendering context
   * @param {Image} [finishLineTexture] - finishline texture
   */
  constructor(model, context, minimapContext, finishLineTexture) {
    this.name = model.name;
    this.coordinates = model.coordinates;
    this.STREET_WIDTH = 65;
    this.startingPositions = model.startingPositions;
    this.powerupPositions = model.powerupPositions;
    this.finishLinePosition = model.finishLinePosition;

    this.context = context;
    this.minimapContext = minimapContext;
    this.finishLineTexture = finishLineTexture;

    this.points = CoordinateCalculation.calculateSmoothTrackCoordinates(
      this.coordinates
    );
    this.outlines = CoordinateCalculation.calculateOutlines(this.coordinates);
    if (this.context && this.minimapContext && this.finishLineTexture) {
      this.mapImage = this.prerender(1);
      this.minimapImage = this.prerender(0.25);
    }
  }

  /**
   * Draw the map from pre-rendered mapImage
   *
   * @returns {void}
   */
  drawMap() {
    this.context.putImageData(this.mapImage, 0, 0);
  }

  /**
   * Prerender static elements with scaling
   *
   * @param {number} [factor=1] - scaling factor
   *
   * @returns {ImageData} - Image of the map
   */
  prerender(factor = 1) {
    let track = CoordinateCalculation.createPath(this.points, factor);
    // Create a new canvas and context
    let canvas = document.createElement("canvas");
    canvas.width = this.outlines.x;
    canvas.height = this.outlines.y;
    let ctx = canvas.getContext("2d");

    CoordinateCalculation.drawStreetLayer(
      ctx,
      "white",
      (this.STREET_WIDTH + 10) * factor,
      track
    ); //outer lines
    CoordinateCalculation.drawStreetLayer(
      ctx,
      "grey",
      this.STREET_WIDTH * factor,
      track
    ); // actual street
    if (factor === 1) {
      CoordinateCalculation.drawStreetLayer(
        ctx,
        "white",
        5 * factor,
        track,
        "square",
        [5, 25]
      ); // dashed center line
    }

    let finishLine = new FinishLine(
      this.finishLinePosition,
      ctx,
      this.finishLineTexture
    );
    finishLine.draw();

    return ctx.getImageData(0, 0, this.outlines.x, this.outlines.y);
  }

  /**
   * Draws the Players on the minimap
   *
   * @param {object} players - players to draw
   *
   * @returns {void}
   */
  updateMinimap(players) {
    this.minimapContext.putImageData(this.minimapImage, 0, 0);
    for (const id in players) {
      if (players.hasOwnProperty(id)) {
        this.minimapContext.fillStyle = players[id].color;
        this.minimapContext.fillRect(
          players[id].position.x * 0.25,
          players[id].position.y * 0.25,
          5,
          5
        );
      }
    }
  }

  /**
   * Get a random map from the predefined maps
   *
   * @returns {object} - Random map
   */
  static getRandomMap() {
    return maps[Math.round(Math.random() * maps.length) % maps.length];
  }
}

module.exports = Map;
