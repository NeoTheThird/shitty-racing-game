const Coordinates = require("./Coordinates");

/**
 * Coordinate helper functions, used by the map
 */
class CoordinateCalculation {
  /**
   * Create a path object from an array of coordinates
   *
   * @param {Coordinates[]} coordinates - Interpolated map coordinates
   * @param {number} [factor=1] - Scaling factor for the path
   *
   * @returns {Path2D} - The smoothed track as a Path2D
   */
  static createPath(coordinates, factor = 1) {
    let ret = new Path2D();

    for (let index = 0; index < coordinates.length; index++) {
      ret.lineTo(coordinates[index].x * factor, coordinates[index].y * factor);
    }
    return ret;
  }

  /**
   * Calculate a smooth curved track using cardinal splines
   *
   * @param {Coordinates[]} path - Key points of the track
   *
   * @returns {Coordinates[]} - The smoothed track as an array of coordinates
   */
  static calculateSmoothTrackCoordinates(path) {
    const tension = 0.7; // Tension parameter for the cardinal spline
    const intersections = 20; // How many points to interpolate
    let cardinals = CoordinateCalculation.getCardinals(intersections);
    const coordinates = [];

    // Make sure the track ends at the beginning
    let _path = [path[path.length - 1], path[path.length - 1]]
      .concat(path)
      .concat([path[0], path[0]]);

    // loop over every given point
    for (let i = 2; i < _path.length - 2; i++) {
      let tvect = {
        current: {
          x: (_path[i + 1].x - _path[i - 1].x) * tension,
          y: (_path[i + 1].y - _path[i - 1].y) * tension
        },
        next: {
          x: (_path[i + 2].x - _path[i].x) * tension,
          y: (_path[i + 2].y - _path[i].y) * tension
        }
      };
      // loop over every interpolated point
      for (let j = 0; j <= intersections; j++) {
        coordinates.push({
          x:
            cardinals[j][0] * _path[i].x +
            cardinals[j][1] * _path[i + 1].x +
            cardinals[j][2] * tvect.current.x +
            cardinals[j][3] * tvect.next.x,
          y:
            cardinals[j][0] * _path[i].y +
            cardinals[j][1] * _path[i + 1].y +
            cardinals[j][2] * tvect.current.y +
            cardinals[j][3] * tvect.next.y
        });
      }
    }
    return coordinates;
  }

  /**
   * Calculate cardinals for cardinal splines for a given number of intersections
   *
   * @param {number} intersections - Number of intersections
   *
   * @returns {number[][]} - Cardinals
   */
  static getCardinals(intersections) {
    let ret = [];
    for (let i = 0; i <= intersections; i++) {
      let step = i / intersections;
      ret.push([
        2 * Math.pow(step, 3) - 3 * Math.pow(step, 2) + 1,
        -(2 * Math.pow(step, 3)) + 3 * Math.pow(step, 2),
        Math.pow(step, 3) - 2 * Math.pow(step, 2) + step,
        Math.pow(step, 3) - Math.pow(step, 2)
      ]);
    }
    return ret;
  }

  /**
   * Calculate outlines of the map from the largest x and y coordinates
   *
   * @param {Coordinates[]} track - Raw track nodes
   *
   * @returns {Coordinates} - Outlines of the map
   */
  static calculateOutlines(track) {
    let ret = {
      x: undefined,
      y: undefined
    };
    for (let i = 0; i < track.length; i++) {
      ret.x = ret.x > track[i].x ? ret.x : track[i].x;
      ret.y = ret.y > track[i].y ? ret.y : track[i].y;
    }
    return {
      x: ret.x + 200,
      y: ret.y + 200
    };
  }

  /**
   * Draw a single layer of the street
   *
   * @param {CanvasRenderingContext2D} ctx - context on the canvas
   * @param {string} color - strokeStyle
   * @param {number} width - lineWidth
   * @param {Path2D} track - Key points of the racetrack on the minimap
   * @param {string} [cap=round] cap - lineCap
   * @param {number[]} [dash=[]] - setLineDash
   *
   * @returns {void}
   */
  static drawStreetLayer(ctx, color, width, track, cap = "round", dash = []) {
    ctx.strokeStyle = color;
    ctx.lineWidth = width;
    ctx.lineCap = cap;
    ctx.setLineDash(dash);
    ctx.stroke(track);
  }
}

module.exports = CoordinateCalculation;
