/**
 * Coordinates on the map, including rotation
 *
 * @property {number} x - x coordinate
 * @property {number} y - y coordinate
 * @property {number} angle - angle in radians (if applicable)
 * @property {number} angle_sin - sine of the angle (if applicable)
 * @property {number} angle_cos - cosine of the angle (if applicable)
 */
class Coordinates {
  /**
   * @constructs Coordinates
   * @param {Coordinates} [coordinates] - Coordinates to expand
   */
  constructor(coordinates = {}) {
    this.x = coordinates.x || 0;
    this.y = coordinates.y || 0;
    this.angle = coordinates.angle || 0;
    this.angle_sin = Math.sin(this.angle);
    this.angle_cos = Math.cos(this.angle);
  }
}

module.exports = Coordinates;
