const Powerup = require("./map/objects/Powerup");
const Car = require("./map/objects/Car");
const Map = require("./map/Map");
const Hurdle = require("./map/objects/Hurdle");
const Collision = require("./map/CollisionDetection");

/**
 * General game class. Originally instantiated by the server.
 *
 * <a href="game.svg">
 *   <img src="game.svg" style="width: 100%;"></img>
 * </a>
 *
 * @property {object} players - All player in this game
 * @property {Hurdle[]} hurdles - Hurdles on the map
 * @property {Powerup[]} powerups - All powerups on the map
 */
class Game {
  /**
   * @constructs Game
   * @param {Map} map - Associated map
   * @param {object} [players] - All player in this game
   * @param {Hurdle[]} [hurdles] - Hurdles on the map
   * @param {Powerup[]} [powerups] - All powerups on the map
   */
  constructor(map, players = {}, hurdles = [], powerups = []) {
    this.map = map;
    this.players = players;
    this.hurdles = hurdles;
    this.powerups = powerups;
    this.active = false;
  }

  /**
   * Remove a car from the map
   *
   * @param {string} id - id to remove
   *
   * @returns {void}
   */
  leave(id) {
    delete this.players[id];
  }

  /**
   * Indicate that a powerup has been collected
   *
   * @param {number} powerupId - ID of collected powerup
   * @param {string} playerId - ID of the player who collected the powerup
   *
   * @returns {void}
   */
  powerupCollected(powerupId, playerId) {
    this.powerups[powerupId].enabled = false;
    this.players[playerId].equippedPowerup = this.powerups[powerupId].type;
  }

  /**
   * Is the game empty?
   *
   * @returns {boolean} - true if the game is empty
   */
  isEmpty() {
    return Object.keys(this.players).length === 0;
  }

  /**
   * Is the game full?
   *
   * @returns {boolean} - true if the game full
   */
  isFull() {
    return (
      Object.keys(this.players).length >= this.map.startingPositions.length
    );
  }

  /**
   * Add a car to the map
   *
   * @param {string} id - player id
   * @param {string} name - username
   * @param {string} color - car color
   * @returns {void}
   */
  join(id, name, color) {
    if (!this.players.hasOwnProperty(id)) {
      if (!this.active && !this.isFull()) {
        this.players[id] = new Car(
          id,
          this.map.startingPositions[Object.keys(this.players).length],
          name,
          color
        );
      } else {
        throw new Error("full");
      }
    }
  }

  /**
   * Create powerups on map
   *
   * @returns {void}
   */
  initializePowerups() {
    for (let i = 0; i < this.map.powerupPositions.length; i++) {
      this.powerups.push(new Powerup(this.map.powerupPositions[i]));
    }
  }

  /**
   * Rank players. Calls back changes and sets new ranks.
   *
   * @param {Function} onRankChange - Called if a rank changed
   *
   * @returns {void}
   */
  rankPlayers(onRankChange) {
    const playerArray = Object.keys(this.players).map(key => {
      const player = this.players[key];
      return [key, player.lastCoordinateHitbox, player.currentRound];
    });

    const rankingArray = playerArray.sort((first, second) => {
      // Sort by round
      if (first[2] > second[2]) {
        return -1;
      } else if (first[2] === second[2]) {
        // Sort by coordinate
        if (first[1] >= second[1]) {
          return -1;
        } else {
          return 1;
        }
      } else {
        return 1;
      }
    });

    // Set player rank
    for (let index = 0; index < rankingArray.length; index++) {
      const playerId = rankingArray[index][0];
      if (index !== this.players[playerId].rank) {
        this.players[playerId].rank = index;
        onRankChange(playerId, index);
      }
    }
  }

  /**
   * Find the first player in the ranking
   *
   * @returns {Car} player - The first player
   */
  findFirstPlayer() {
    for (const id in this.players) {
      if (this.players.hasOwnProperty(id)) {
        const player = this.players[id];

        if (player.rank === 0) {
          return player;
        }
      }
    }
  }

  /**
   * Indicate if collision with a hurdle happened
   *
   * @param {Car} player - The collided player
   * @param {Function} onCollision - Handling on collision
   *
   * @returns {void}
   */
  didPlayerCollideWithHurdle(player, onCollision) {
    const hitHurdleId = Collision.detect(player, this.hurdles, 10, true);

    if (
      hitHurdleId !== -1 &&
      this.hurdles[hitHurdleId].placedBy !== player.id
    ) {
      this.hurdles.splice(hitHurdleId, 1);
      onCollision(hitHurdleId);
    }
  }

  /**
   * Place a hurdle on the map on powerup activation
   *
   * @param {Coordinates} position - Position where the hurdle should be placed
   * @param {string} placedByID - ID of the player who placed the hurdle
   *
   * @returns {Hurdle} - The newly created hurdle
   */
  placeHurdle(position, placedByID) {
    const newHurdle = new Hurdle(position, placedByID);
    this.hurdles.push(newHurdle);
    return newHurdle;
  }
}

module.exports = Game;
