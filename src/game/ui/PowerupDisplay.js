const POWERUP_TYPES = require("../map/objects/POWERUP_TYPES");

/**
 * Powerup display manager
 *
 * @property {CanvasRenderingContext2D} powerupContext - the context which is drawn on the canvas
 */
class PowerupDisplay {
  /**
   * @constructs PowerupDisplay
   * @param {CanvasRenderingContext2D} powerupCanvas - the context which is drawn on the canvas
   * @param {Image} [texture] - texture
   */
  constructor(powerupCanvas, texture) {
    this.powerupCanvas = powerupCanvas;
    this.powerupContext = powerupCanvas.getContext("2d");
    this.texture = texture;
    this.imageSize = 300;
    this.textureSize = 75;
    this.currentAnimationStep = 0;
  }

  /**
   * Draw the powerup display
   *
   * @param {string} powerup - Type of the equipped Powerup
   *
   * @returns {void}
   */
  draw(powerup) {
    this.currentAnimationStep = (this.currentAnimationStep + 1) % 10;
    this.powerupContext.clearRect(
      0,
      0,
      this.powerupCanvas.width,
      this.powerupCanvas.height
    );
    if (powerup) {
      let spriteSelector = 0;
      if (powerup === POWERUP_TYPES.HURDLE) {
        spriteSelector = 1;
      } else if (powerup === POWERUP_TYPES.SLOW_FIRST) {
        spriteSelector = 2;
      }

      this.powerupContext.drawImage(
        this.texture,
        this.currentAnimationStep * this.textureSize,
        spriteSelector * this.textureSize,
        this.imageSize,
        this.imageSize,
        0,
        0,
        this.imageSize,
        this.imageSize
      );
    }
  }
}
module.exports = PowerupDisplay;
