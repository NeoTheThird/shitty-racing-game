const Player = require("../map/objects/Car");

/**
 * GameEnding manager
 */
class GameEnding {
  /**
   * @constructs GameEnding
   * @param {Player[]} players - player array
   * @param {HTMLDivElement} gameEnding - div
   */
  constructor(players, gameEnding) {
    this.players = players;
    this.gameEnding = gameEnding;
  }

  /**
   * Create the game-ending screen
   *
   * @returns {void}
   */
  createGameEnding() {
    this.gameEnding.style.width = "800px";
    this.gameEnding.style.height = "449px";
    let divbody = "<img src='images/medaillen.png'/>";
    for (const id in this.players) {
      if (this.players.hasOwnProperty(id)) {
        const player = this.players[id];
        switch (player.rank + 1) {
          case 1:
            divbody +=
              "<img src='images/" +
              player.color +
              ".png' style='position:absolute; top:303px; left:376px; z-index:10;'/><br><p style='position:absolute; top:329px; left:374px; z-index:10; color: black'>" +
              player.name +
              "</p>";
            break;
          case 2:
            divbody +=
              "<img src='images/" +
              player.color +
              ".png' style='position:absolute; top:254px; left:132px; z-index:10;'/><br><p style='position:absolute; top:281px; left:128px; z-index:10; color: black'>" +
              player.name +
              "</p>";
            break;
          default:
            divbody +=
              "<img src='images/" +
              player.color +
              ".png' style='position:absolute; top:257px; left:614px; z-index:10;'/><br><p style='position:absolute; top:284px; left:610px; z-index:10; color: black'>" +
              player.name +
              "</p>";
        }
      }
    }
    this.gameEnding.innerHTML = divbody;
  }
}

module.exports = GameEnding;
