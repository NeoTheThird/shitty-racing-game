const Car = require("../map/objects/Car");

/**
 * Leaderboard manager
 */
class Leaderboard {
  /**
   * @constructs Leaderboard
   * @param {Car[]} players - player array
   * @param {HTMLTableElement} leaderboard - table
   */
  constructor(players, leaderboard) {
    this.players = players;
    this.leaderboard = leaderboard;
  }

  /**
   * Create the leaderboard with all connected playes
   *
   * @returns {void}
   */
  createLeaderboard() {
    let tbody = this.leaderboard.querySelector("tbody");
    let tbodyHtml = "";

    for (const id in this.players) {
      if (this.players.hasOwnProperty(id)) {
        const player = this.players[id];
        if (player.rank === -1) {
          player.rank = "-";
        }
        tbodyHtml +=
          "<tr><td id='" +
          player.id +
          "'>" +
          player.rank +
          "</td><td><img src='images/" +
          player.color +
          ".png'/></td><td>" +
          player.name +
          "</td></tr>";
      }
    }
    tbody.innerHTML = tbodyHtml;
  }

  /**
   * Update the player rank
   *
   * @param {Car[]} players - All connected players
   *
   * @returns {void}
   */
  update(players) {
    const array = Object.keys(players).map(key => {
      return [
        players[key].id,
        players[key].name,
        players[key].rank + 1,
        players[key].color
      ];
    });

    for (let i = 0; i < array.length; i++) {
      let tbodyHtml = "";
      const player = array[i];
      let row = this.leaderboard.querySelector(`#${player[0]}`);
      tbodyHtml = player[2];
      row.innerHTML = player[2];
    }
    this.sortTable();
  }

  /**
   * Sort the table by ranks
   *
   * @returns {void}
   */
  sortTable() {
    let switching = true;
    let shouldSwitch = false;
    let i = 0;
    while (switching) {
      switching = false;
      let tableRows = this.leaderboard.rows;

      for (i = 1; i < tableRows.length - 1; i++) {
        shouldSwitch = false;
        let x = tableRows[i].getElementsByTagName("td")[0];
        let y = tableRows[i + 1].getElementsByTagName("td")[0];

        if (Number(x.innerHTML) > Number(y.innerHTML)) {
          shouldSwitch = true;
          break;
        }
      }

      if (shouldSwitch) {
        tableRows[i].parentNode.insertBefore(tableRows[i + 1], tableRows[i]);
        switching = true;
      }
    }
  }
}

module.exports = Leaderboard;
