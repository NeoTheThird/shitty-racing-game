/**
 * Manager for the speedometer display
 *
 * @property {CanvasRenderingContext2D} speedometerContext - the context which is drawn on the canvas
 */
class Speedometer {
  /**
   * @constructs Speedometer
   * @param {CanvasRenderingContext2D} speedometerCanvas - the context which is drawn on the canvas
   */
  constructor(speedometerCanvas) {
    this.pathCircle1;
    this.pathCircle2;
    this.speedometerContext = speedometerCanvas.getContext("2d");
    this.needleCenter = { x: 75, y: 110 };
    this.needleTop = { x: 20, y: 110 };
    this.needleLength = 55;
    this.radiusCircle1 = 70;
    this.radiusCircle2 = 60;
    this.speedometerImage = this.renderSpeedometer();
  }

  /**
   * Draw the speedometer from pre-rendered speedometerImage
   *
   * @returns {void}
   */
  draw() {
    this.speedometerContext.putImageData(this.speedometerImage, 0, 0);
  }

  /**
   * Draw the whole Speedometer gauge
   *
   * @param {CanvasRenderingContext2D} ctx - context on the canvas
   *
   * @returns {void}
   */
  constructSpeedometer(ctx) {
    //Draws the bigger first halfcircle of the speedometer
    let pCircle1 = new Path2D();
    ctx.beginPath();
    pCircle1.arc(
      this.needleCenter.x,
      this.needleCenter.y,
      this.radiusCircle1,
      0,
      Math.PI,
      true
    );
    pCircle1.closePath();
    ctx.fillStyle = "gray";
    ctx.fill(pCircle1);
    this.pathCircle1 = pCircle1;

    //Draws the smaller second halfcircle of the speedometer
    let pCircle2 = new Path2D();
    ctx.beginPath();
    pCircle2.arc(
      this.needleCenter.x,
      this.needleCenter.y,
      this.radiusCircle2,
      0,
      Math.PI,
      true
    );
    pCircle2.closePath();
    ctx.fillStyle = "white";
    ctx.fill(pCircle2);
    this.pathCircle2 = pCircle2;

    //Draws 9 marks on on the halfcircles

    for (let iMark = 0; iMark <= Math.PI; iMark += (1 / 8) * Math.PI) {
      ctx.beginPath();
      ctx.moveTo(
        this.needleCenter.x - Math.cos(iMark) * this.radiusCircle1,
        this.needleCenter.y - Math.sin(iMark) * this.radiusCircle1
      );
      ctx.lineTo(
        this.needleCenter.x - Math.cos(iMark) * this.radiusCircle2,
        this.needleCenter.y - Math.sin(iMark) * this.radiusCircle2
      );
      ctx.strokeStyle = "white";
      ctx.stroke();
      ctx.closePath();
    }
  }

  /**
   * Calculate the actual position of the needle and draws it
   *
   * @param {number} speed - The current speed of the player
   * @param {number} maxSpeed - The maximum speed the player can reach
   *
   * @returns {void}
   */
  moveNeedle(speed, maxSpeed) {
    //Draws the needle of the speedometer
    maxSpeed += 2;
    this.speedometerContext.beginPath();
    this.speedometerContext.moveTo(this.needleCenter.x, this.needleCenter.y);
    this.speedometerContext.lineTo(this.needleTop.x, this.needleTop.y);
    this.speedometerContext.lineWidth = 3;
    this.speedometerContext.strokeStyle = "red";
    this.speedometerContext.stroke();
    this.speedometerContext.closePath();

    //Moves the needle according to the speed to a new position
    this.needleTop.x =
      this.needleCenter.x -
      Math.cos(Math.abs(speed / maxSpeed) * (0.8 * Math.PI)) *
        this.needleLength;
    this.needleTop.y =
      this.needleCenter.y -
      Math.sin(Math.abs(speed / maxSpeed) * (0.8 * Math.PI)) *
        this.needleLength;
  }

  /**
   * Render the speedometer
   *
   * @returns {ImageData} - Image of the speedometer
   */
  renderSpeedometer() {
    // Create a new canvas and context
    let canvas = document.createElement("canvas");
    canvas.width = 150;
    canvas.height = 110;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    this.constructSpeedometer(ctx);
    return ctx.getImageData(0, 0, canvas.width, canvas.height);
  }
}
module.exports = Speedometer;
