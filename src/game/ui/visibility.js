/**
 * Show a hidden HTMLElement
 *
 * @param {HTMLElement} element - Element to show
 *
 * @returns {void}
 */
function show(element) {
  element.classList.remove("hidden");
}

/**
 * Hide an HTMLElement
 *
 * @param {HTMLElement} element - Element to hide
 *
 * @returns {void}
 */
function hide(element) {
  element.classList.add("hidden");
}

/**
 * Toggle visibility of an HTMLElement
 *
 * @param {HTMLElement} element - Element to show/hide
 *
 * @returns {void}
 */
function toggle(element) {
  element.classList.toggle("hidden");
}

module.exports = {
  show: show,
  hide: hide,
  toggle: toggle
};
