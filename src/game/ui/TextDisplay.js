/**
 * Manager for displaying the text
 *
 * @property {Text} textDisplay - display text
 */

class TextDisplay {
  /**
   * @constructs TextDisplay
   * @param {Text} textDisplay - text that should display something
   */
  constructor(textDisplay) {
    this.textDisplay = textDisplay;
  }

  /**
   * Update the current round number
   *
   * @param {number} round - the current round of the player
   *
   * @returns {void}
   */
  setCurrentRound(round) {
    this.textDisplay.innerHTML = round + " / " + 3;
  }

  /**
   * Update the current player rank
   *
   * @param {number} rank - the rank of the player
   *
   * @returns {void}
   */
  setCurrentRank(rank) {
    const displayRank = rank + 1;
    this.textDisplay.innerHTML = "Rank: " + displayRank;
  }
}

module.exports = TextDisplay;
