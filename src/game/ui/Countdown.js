/**
 * Countdown manager
 *
 * @property {HTMLImageElement} countdownImages - Images of the countdown
 * @property {Audio} countdownSoundStart - sound of the countdown at the start
 * @property {Audio} countdownSoundEnd - sound of the countdown at the end
 */
class Countdown {
  /**
   * @constructs Countdown
   * @param {HTMLImageElement} countdownImages - Images of the countdown
   * @param {Audio} countdownSoundStart - sound of the countdown at the start
   * @param {Audio} countdownSoundEnd - sound of the countdown at the end
   */
  constructor(countdownImages, countdownSoundStart, countdownSoundEnd) {
    this.countdownImages = countdownImages;
    this.countdownSoundStart = countdownSoundStart;
    this.countdownSoundEnd = countdownSoundEnd;
  }

  /**
   * Step in the countdown
   *
   * @param {number} step - countdown step
   * @param {number} [timeout] - how long to show the last step, used for testing
   *
   * @returns {void}
   */
  step(step, timeout = 500) {
    this.countdownImages.style.display = "block";
    this.countdownImages.src = "images/countdown" + step + ".png";
    if (step < 4) {
      this.countdownSoundStart.play();
    } else {
      setTimeout(() => {
        this.countdownImages.parentNode.removeChild(this.countdownImages);
      }, timeout);
      this.countdownSoundEnd.play();
    }
  }
}
module.exports = Countdown;
