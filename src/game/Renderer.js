const Powerup = require("./map/objects/Powerup");
const Player = require("./map/objects/Car");
const Hurdle = require("./map/objects/Hurdle");
const Map = require("./map/Map");
const Game = require("./Game");
const Speedometer = require("./ui/Speedometer");
const Coordinates = require("./map/Coordinates");
const PowerupDisplay = require("./ui/PowerupDisplay");
const Leaderboard = require("../game/ui/Leaderboard");
const Countdown = require("./ui/Countdown");
const visibility = require("./ui/visibility");
const TextDisplay = require("./ui/TextDisplay");
const Explosion = require("./map/objects/Explosion");
const Car = require("./map/objects/Car");
const GameEnding = require("../game/ui/GameEnding");

/**
 * Client-side rendering
 *
 * <a href="game.svg">
 *   <img src="game.svg" style="width: 100%;"></img>
 * </a>
 */
class Renderer {
  /**
   * @constructs Renderer
   * @param {Game} game - Game object
   * @param {string} id - player id
   * @param {object[]} assets - Assets from AssetLoader
   * @param {HTMLCanvasElement} view - Mainview canvas
   * @param {HTMLCanvasElement} speedometerCanvas - the canvas which contains the speedometer
   * @param {HTMLCanvasElement} minimapCanvas - Minimap canvas
   * @param {HTMLCanvasElement} powerupCanvas - Powerup Display canvas
   * @param {HTMLImageElement} countdownImages - Images of the countdown
   * @param {HTMLTableElement} leaderboardTable - Leaderboard
   * @param {Text} roundCounter - Round Display
   * @param {Text} rankingCounter - Ranking Display
   * @param {HTMLDivElement} gameEndingDiv - Game-Ending
   */
  constructor(
    game,
    id,
    assets,
    view,
    speedometerCanvas,
    minimapCanvas,
    powerupCanvas,
    countdownImages,
    leaderboardTable,
    roundCounter,
    rankingCounter,
    gameEndingDiv
  ) {
    this.offscreenCanvas = document.createElement("canvas");
    this.offscreenCanvas.width = 1000;
    this.offscreenCanvas.height = 1000;
    this.offscreenCanvasContext = this.offscreenCanvas.getContext("2d");
    this.minimapContext = minimapCanvas.getContext("2d");
    this.frameCount = 0;
    this.id = id;
    this.assets = assets;
    this.view = view;
    this.viewContext = this.view.getContext("2d");
    this.animationFrame; // holds the id returned by requestAnimationFrame()
    this.speedometer = new Speedometer(speedometerCanvas);
    this.uiElements = [
      view,
      speedometerCanvas,
      minimapCanvas,
      powerupCanvas,
      leaderboardTable
    ];

    this.game = new Game(
      new Map(
        game.map,
        this.offscreenCanvasContext,
        this.minimapContext,
        assets.finishline
      ),
      game.players,
      game.hurdles,
      game.powerups
    );

    this.leaderboard = new Leaderboard(this.game.players, leaderboardTable);
    this.gameEnding = new GameEnding(this.game.players, gameEndingDiv);
    this.roundDisplay = new TextDisplay(roundCounter);
    this.rankingDisplay = new TextDisplay(rankingCounter);

    this.audio = {
      alarm: this.assets.alarm,
      collect: this.assets.collect,
      explosion: this.assets.explosionSound,
      activation: this.assets.activation,
      engine: this.assets.engine,
      countdownSoundStart: this.assets.countdownSoundStart,
      countdownSoundEnd: this.assets.countdownSoundEnd,
      finishSound: this.assets.finishSound
    };

    this.game.powerups = this.game.powerups.map(
      powerup =>
        new Powerup(
          powerup.position,
          powerup.type,
          powerup.enabled,
          this.offscreenCanvasContext,
          this.assets.coins
        )
    );

    for (const id in this.game.players) {
      if (this.game.players.hasOwnProperty(id)) {
        this.game.players[id] = new Player(
          this.game.players[id].id,
          this.game.players[id].position,
          this.game.players[id].name,
          this.game.players[id].color,
          this.offscreenCanvasContext,
          assets[this.game.players[id].color]
        );
      }
    }

    this.game.hurdles = this.game.hurdles.map(
      hurdle =>
        new Hurdle(
          hurdle.position,
          hurdle.placedBy,
          this.offscreenCanvasContext,
          this.assets.bombs
        )
    );

    this.powerupDisplay = new PowerupDisplay(powerupCanvas, this.assets.items);

    this.countdown = new Countdown(
      countdownImages,
      this.assets.countdownSoundStart,
      this.assets.countdownSoundEnd
    );

    this.leaderboard.createLeaderboard();

    this.explosion = new Explosion(
      this.offscreenCanvasContext,
      this.assets.explosion
    );

    window.addEventListener("resize", this.resizeView);

    this.start();
  }

  /**
   * Show the game ended screen
   *
   * @returns {void}
   */
  showGameEnd() {
    this.gameEnding.createGameEnding();
    this.assets.finishSound.play();
  }

  /**
   * Draw the powerups on the map
   *
   * @returns {void}
   */
  powerupDraw() {
    this.frameCount++;

    for (let i = 0; i < this.game.powerups.length; i++) {
      if (this.game.powerups[i].enabled) {
        this.game.powerups[i].update(this.frameCount);
        this.game.powerups[i].draw();
      }
    }
  }

  /**
   * Draw the hurdles on the map
   *
   * @returns {void}
   */
  hurdleDraw() {
    for (let i = 0; i < this.game.hurdles.length; i++) {
      this.game.hurdles[i].update(this.frameCount);
      this.game.hurdles[i].draw();
    }
  }

  /**
   * Control all connected players
   *
   * @param {Car[]} players - All connected players
   *
   * @returns {void}
   */
  control(players) {
    let newRanks = false;
    for (const playerId in players) {
      if (players.hasOwnProperty(playerId)) {
        if (this.game.players[playerId].rank !== players[playerId].rank) {
          newRanks = true;
        }
        this.game.players[playerId].updateWith(players[playerId]);
      }
    }
    if (newRanks) {
      this.leaderboard.update(this.game.players);
    }
  }

  /**
   * Add hurdle on map
   *
   * @param {object} hurdle - The hurdle information
   *
   * @returns {void}
   */
  addHurdle(hurdle) {
    this.game.hurdles.push(
      new Hurdle(
        hurdle.position,
        hurdle.placedBy,
        this.offscreenCanvasContext,
        this.assets.bombs
      )
    );
  }

  /**
   * Show an explosion on the road
   *
   * @param {Coordinates} position - Position of the explosion
   *
   * @returns {void}
   */
  showExplosionAt(position) {
    this.assets.explosionSound.play();
    this.explosion.explodeAt(position);
  }

  /**
   * Remove hurdle from the road after collision
   *
   * @param {object} data - Hurdle and player
   *
   * @returns {void}
   */
  collidedWithHurdle(data) {
    this.game.hurdles.splice(data.hurdleId, 1);
    this.showExplosionAt(this.game.players[data.playerId].position);
  }

  /**
   * Changes engine sound if speed gets higher
   *
   * @returns {void}
   */
  changeEngineSound() {
    this.audio.engine.volume = Math.max(
      Math.min(this.game.players[this.id].speed * 0.8, 1),
      0.2
    );
    this.audio.engine.play();
  }

  /**
   * Resize the the map view dynamically
   *
   * @returns {void}
   */
  resizeView() {
    this.view.width = document.documentElement.clientWidth / 2;
    this.view.height = document.documentElement.clientHeight / 2;
  }

  /**
   * Render main view. Looped using requestAnimationFrame().
   *
   * @returns {void}
   */
  render() {
    this.animationFrame = requestAnimationFrame(() => this.render());
    // prerendering
    this.game.map.drawMap();
    this.powerupDraw();
    this.hurdleDraw();
    this.resizeView();
    for (const id in this.game.players) {
      if (this.game.players.hasOwnProperty(id)) {
        this.game.players[id].draw();
      }
    }
    if (this.explosion.collidedWithHurdle) {
      this.explosion.update(this.frameCount);
      this.explosion.draw();
      setTimeout(() => (this.explosion.collidedWithHurdle = false), 500);
    }

    // clear the canvas
    this.viewContext.setTransform(1, 0, 0, 1, 0, 0);
    this.viewContext.clearRect(0, 0, this.view.width, this.view.height);

    // evil 2d-transformation matrix to move and rotate around the center of the screen
    // view rotation = -(player rotation) - 90°
    // it has taken like ten years, but math is finally awesome!
    this.viewContext.setTransform(
      -this.game.players[this.id].renderPosition.angle_sin, // cos(-x-90°) = -sin(x)
      -this.game.players[this.id].renderPosition.angle_cos, // sin(-x-90°) = -cos(x)
      this.game.players[this.id].renderPosition.angle_cos, // -sin(-x-90°) = cos(x)
      -this.game.players[this.id].renderPosition.angle_sin, // cos(-x-90°) = -sin(x)
      this.view.width * 0.5,
      this.view.height * 0.8
    );
    this.viewContext.drawImage(
      this.offscreenCanvas,
      -this.game.players[this.id].position.x,
      -this.game.players[this.id].position.y
    );
  }

  /**
   * Render the UI elements
   *
   * @returns {void}
   */
  renderUi() {
    this.changeEngineSound();
    this.powerupDisplay.draw(this.game.players[this.id].equippedPowerup);
  }

  /**
   * Render the low priority canvases
   *
   * @returns {void}
   */
  renderLowPrio() {
    this.speedometer.draw();
    this.speedometer.moveNeedle(
      this.game.players[this.id].speed,
      this.game.players[this.id].VMAX
    );
    this.game.map.updateMinimap(this.game.players);
  }

  /**
   * Start the logic- and rendering loops
   *
   * @returns {void}
   */
  start() {
    this.uiElements.forEach(visibility.show);
    this.render();
    this.uiElementsInterval = setInterval(this.renderUi.bind(this), 150);
    this.lowPriorityInterval = setInterval(this.renderLowPrio.bind(this), 50);
  }

  /**
   * Stop rendering
   *
   * @returns {void}
   */
  stop() {
    cancelAnimationFrame(this.animationFrame);
    clearInterval(this.uiElementsInterval);
    clearInterval(this.lowPriorityInterval);
  }
}

module.exports = Renderer;
