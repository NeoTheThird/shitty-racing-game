/**
 * Key storage manager
 *
 * @property {object} storage - storage of pressed keys
 */
class KeyStorage {
  /**
   * @constructs KeyStorage
   */
  constructor() {
    this.storage = {};
  }

  /**
   * Query whether a key is pressed
   *
   * @param {string} code - string referencing a key
   *
   * @returns {boolean} - Is the key pressed?
   */
  isDown(code) {
    return this.storage[code] || false;
  }

  /**
   * To be connected to the keydown event listener
   *
   * @param {string} code - string referencing a key as returned by the keydown event listener
   *
   * @returns {void}
   */
  onKeydown(code) {
    this.storage[code] = true;
  }

  /**
   * To be connected to the keyup event listener
   *
   * @param {string} code - string referencing a key as returned by the keyup event listener
   *
   * @returns {void}
   */
  onKeyup(code) {
    delete this.storage[code];
  }

  /**
   * Query whether the space bar is pressed
   *
   * @returns {boolean} - should the powerup get activated
   */
  isSpace() {
    return this.isDown("Space");
  }

  /**
   * Query whether the car should go forwards
   *
   * @returns {boolean} - should the car go forwards?
   */
  goForwards() {
    return (
      (this.isDown("KeyW") || this.isDown("ArrowUp")) &&
      !(this.isDown("KeyS") || this.isDown("ArrowDown"))
    );
  }

  /**
   * Query whether the car should go backwards
   *
   * @returns {boolean} - should the car go backwards?
   */
  goBackwards() {
    return (
      (this.isDown("KeyS") || this.isDown("ArrowDown")) &&
      !(this.isDown("KeyW") || this.isDown("ArrowUp"))
    );
  }

  /**
   * Query whether the car should go right
   *
   * @returns {boolean} - should the car go right?
   */
  goRight() {
    return (
      (this.isDown("KeyD") || this.isDown("ArrowRight")) &&
      !(this.isDown("KeyA") || this.isDown("ArrowLeft"))
    );
  }

  /**
   * Query whether the car should go left
   *
   * @returns {boolean} - should the car go left?
   */
  goLeft() {
    return (
      (this.isDown("KeyA") || this.isDown("ArrowLeft")) &&
      !(this.isDown("KeyD") || this.isDown("ArrowRight"))
    );
  }
}

module.exports = KeyStorage;
