/**
 * Settings ui manager
 *
 * @param {HTMLElement} username - username
 * @param {object} color - color settings elements
 * @param {HTMLElement} color.orange - username
 * @param {HTMLElement} color.black - username
 * @param {HTMLElement} color.blue - username
 * @param {HTMLElement} color.yellow - username
 */
class Settings {
  /**
   * @constructs Settings
   */
  constructor() {
    this.username = document.getElementById("settings_username");
    this.color = {
      orange: document.getElementById("settings_color_orange"),
      black: document.getElementById("settings_color_black"),
      blue: document.getElementById("settings_color_blue"),
      yellow: document.getElementById("settings_color_yellow")
    };

    // init
    this.username.value =
      window.localStorage.getItem("username") ||
      "User " + Math.round(Math.random() * 1000);
    if (window.localStorage.getItem("color")) {
      this.color[window.localStorage.getItem("color")].checked = true;
    }
  }

  /**
   * Read color from localStorage
   *
   * @returns {string} - color
   */
  getColor() {
    return this.color.orange.checked
      ? "orange"
      : this.color.black.checked
      ? "black"
      : this.color.blue.checked
      ? "blue"
      : "yellow";
  }

  /**
   * Store settings in localStorage
   *
   * @returns {void}
   */
  store() {
    window.localStorage.setItem("username", this.username.value);
    window.localStorage.setItem("color", this.getColor());
  }
}

module.exports = Settings;
