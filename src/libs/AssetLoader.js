/**
 * Preload assets synchronously
 */
class AssetLoader {
  /**
   * Load a single asset
   *
   * @param {string} name - will be returned as the property name
   * @param {string} url - where to load the resource
   * @param {string} type - Image or Audio
   *
   * @returns {Promise<string, HTMLElement>} - promise for the requested assets
   */
  static loadAsset(name, url, type) {
    switch (type) {
      case "image":
        return new Promise(resolve => {
          const image = new Image();
          image.src = url;
          image.addEventListener("load", function() {
            return resolve({ name, asset: this });
          });
        });
      case "audio":
        return new Promise(resolve => {
          const audio = new Audio(url);
          audio.addEventListener("loadeddata", function() {
            return resolve({ name, asset: this });
          });
        });
      default:
        return Promise.reject(new Error("Unknown type"));
    }
  }

  /**
   * @class AssetToLoad
   * @classdesc An asset to be loaded by the assetloader
   * @property {string} name - will be returned as the property name
   * @property {string} url - where to load the resource
   * @property {string} type - Image or Audio
   */

  /**
   * Load assets
   *
   * @param {AssetToLoad[]} assetsToLoad - assets to load
   *
   * @returns {Promise<object>} - promise for the requested assets with their names as keys
   */
  static loadAssets(assetsToLoad) {
    return Promise.all(
      assetsToLoad.map(asset =>
        AssetLoader.loadAsset(asset.name, asset.url, asset.type)
      )
    )
      .then(assets =>
        assets.reduceRight(
          (acc, elem) => ({
            ...acc,
            [elem.name]: elem.asset
          }),
          {}
        )
      )
      .catch(error => {
        throw new Error("Not all assets could be loaded: " + error);
      });
  }
}
module.exports = AssetLoader;
