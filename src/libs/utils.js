/**
 * Return url parameters. Unused until rooms are implemented.
 *
 * @returns {object[]} - url parameters
 */
function getUrlParameters() {
  let ret = {};
  window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    (_, property, value) => {
      ret[property] = value;
    }
  );
  return ret;
}

module.exports = {
  getUrlParameters: getUrlParameters
};
