/**
 * Handler for displaying notification messages
 *
 * @property {HTMLDivElement} queue - alert queue div container
 */
class AlertQueue {
  /**
   * @constructs AlertQueue
   *
   * @param {HTMLDivElement} queue - alert queue div container
   */
  constructor(queue) {
    this.queue = queue;
  }

  /**
   * Show a message for an optional amount of time
   *
   * @param {string} text - The text to display
   * @param {string} [color=red] - Color of the popup
   * @param {number} [hideAfter=3000] - Time of visibility (in ms)
   *
   * @returns {void}
   */
  alert(text, color = "red", hideAfter = 3000) {
    let alertBox = document.createElement("div");
    alertBox.classList.add("alert-box");
    alertBox.style.backgroundColor = color;
    alertBox.innerText = text;

    this.queue.appendChild(alertBox);

    if (hideAfter !== undefined) {
      setTimeout(() => {
        this.queue.removeChild(alertBox);
      }, hideAfter);
    }
  }
}

module.exports = AlertQueue;
