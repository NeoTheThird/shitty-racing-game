/**
 * @enum MESSAGES
 * @description Enum for mapping websockets messages to strings
 */
const MESSAGES = {
  CONTROL: "control",
  COUNTDOWN_STEP: "countdownStep",
  GAME_IS_FULL: "full",
  GAME_STARTED: "gameStarted",
  HURDLE_ACTIVATED: "hurdleActivated",
  HURDLE_COLLISION: "hurdleCollision",
  JOIN_GAME: "joinGame",
  LEAVE_GAME: "leaveGame",
  NEXT_ROUND: "nextRound",
  OUT_OF_BOUNDS: "outOfBounds",
  PLAYER_FINISHED: "playerFinished",
  PLAYER_JOINED: "playerJoined",
  PLAYER_LEFT: "playerLeft",
  POSITION_CHANGED: "newPosition",
  POWERUP_COLLECTED: "collectedPowerup",
  POWERUP_VISIBLE_AGAIN: "powerupVisibleAgain",
  RANK_CHANGED: "rankChanged",
  READY: "ready",
  RESET_TO_CHECKPOINT: "resetToCheckpoint",
  SLOW_FIRST: "slowFirst",
  TRIGGER_POWERUP: "powerupTriggered",
  WAIT_FOR_PLAYERS: "wait"
};

module.exports = MESSAGES;
