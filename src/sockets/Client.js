const KeyStorage = require("../libs/KeyStorage");
const Renderer = require("../game/Renderer");
const AlertQueue = require("../libs/AlertQueue");
const Settings = require("../libs/Settings");
const log = require("debug")("client");
const visibility = require("../game/ui/visibility");

const io = require("socket.io-client")({
  forceNew: true,
  autoConnect: false,
  reconnectionAttempts: 5,
  timeout: 1000
});
const MESSAGES = require("./MESSAGES");

/**
 * The game client. The root object instantiated in the browser.
 *
 * <a href="client.svg">
 *   <img src="client.svg" style="width: 100%;"></img>
 * </a>
 *
 * @property {Settings} settings - Settings manager
 * @property {object[]} assets - Pre-loaded assets
 * @property {Renderer} renderer - The local Renderer instance
 * @property {object} socket - socket.io client
 * @property {KeyStorage} keyStorage - keypress manager
 * @property {AlertQueue} alertQueue - alert queue manager
 */
class Client {
  /**
   * @constructs Client
   * @param {object[]} assets - Assets from AssetLoader
   */
  constructor(assets) {
    this.settings = new Settings();
    this.assets = assets;
    this.renderer;
    this.socket = io;
    this.keyStorage = new KeyStorage();

    this.alertQueue = new AlertQueue(document.getElementById("alertQueue"));

    this.addEventListeners();
    this.connectSockets();
    this.connect();
  }

  /**
   * Add event listeners
   *
   * @returns {void}
   */
  addEventListeners() {
    window.addEventListener("beforeunload", () => {
      this.socket.emit(MESSAGES.LEAVE_GAME);
      this.socket.disconnect();
    });

    document.addEventListener("keydown", event => {
      try {
        this.keyStorage.onKeydown(event.code);
        if (this.renderer.game.active) {
          this.control(this.keyStorage.storage);
        }
      } catch (error) {}
    });

    document.addEventListener("keyup", event => {
      try {
        this.keyStorage.onKeyup(event.code);
        if (this.renderer.game.active) {
          this.control(this.keyStorage.storage);
        }
      } catch (error) {}
    });

    document.getElementById("join_button").onclick = () => {
      this.settings.store();
      visibility.hide(document.getElementById("settingsModal"));
      this.socket.emit(MESSAGES.JOIN_GAME, {
        name: this.settings.username.value,
        color: this.settings.getColor()
      });
    };
  }

  /**
   * Render the game locally
   *
   * @param {object} game - Game object
   *
   * @returns {void}
   */
  renderGame(game) {
    visibility.hide(document.getElementById("splash"));
    document.body.classList.remove("background-box");
    this.renderer = new Renderer(
      game,
      io.id,
      this.assets,
      document.getElementById("view"),
      document.getElementById("speedometer"),
      document.getElementById("minimap"),
      document.getElementById("powerup"),
      document.getElementById("countdown"),
      document.getElementById("leaderboard"),
      document.getElementById("roundCounter"),
      document.getElementById("rankingCounter"),
      document.getElementById("gameEnding")
    );
    this.renderer.game.players[io.id].keyStorage = this.keyStorage;
    this.renderer.roundDisplay.setCurrentRound(1);
    this.renderer.rankingDisplay.setCurrentRank(0);

    this.alertQueue.alert(
      `Race starting with ${Object.keys(game.players).length} players...`
    );
  }

  /**
   * Fatal errors that need reloading
   *
   * @param {string} error - what error to show (full, connection-lost, etc)
   *
   * @returns {void}
   */
  fatalError(error) {
    window.location.href = "/?error=" + error;
  }

  /**
   * Connect to server and specifiy onReceive actions
   *
   * @returns {void}
   */
  connect() {
    this.socket = io.connect();
  }

  /**
   * Send changed keys to server
   *
   * @param {object} keys - Pressed keys
   *
   * @fires Client#CONTROL
   *
   * @returns {void}
   */
  control(keys) {
    /**
     * send pressed keys to server
     *
     * @event Client#CONTROL
     * @type {object}
     */
    this.socket.emit(MESSAGES.CONTROL, keys);
  }

  /**
   * Actions which should be triggered if new messages are received
   *
   * @listens Server#COUNTDOWN_STEP
   * @listens Server#GAME_IS_FULL
   * @listens Server#GAME_STARTED
   * @listens Server#HURDLE_ACTIVATED
   * @listens Server#HURDLE_COLLISION
   * @listens Server#NEXT_ROUND
   * @listens Server#OUT_OF_BOUNDS
   * @listens Server#PLAYER_FINISHED
   * @listens Server#PLAYER_JOINED
   * @listens Server#PLAYER_LEFT
   * @listens Server#POSITION_CHANGED
   * @listens Server#POWERUP_COLLECTED
   * @listens Server#POWERUP_VISIBLE_AGAIN
   * @listens Server#RANK_CHANGED
   * @listens Server#SLOW_FIRST
   * @listens Server#RESET_TO_CHECKPOINT
   * @listens Server#TRIGGER_POWERUP
   * @listens Server#WAIT_FOR_PLAYERS
   * @returns {void}
   */
  connectSockets() {
    let _this = this;
    this.socket.on("connect", function() {
      visibility.show(document.getElementById("settingsModal"));
      log(`Client successfully connected as ${io.id}`);
    });

    this.socket.on("connect_timeout", function() {
      _this.fatalError("full");
    });

    this.socket.on("reconnect_error", function() {
      log("Client failed to reconnect once");
      _this.alertQueue.alert("Server does not respond!");
    });

    this.socket.on("reconnect_failed", function() {
      log("Client failed to connect completely");
      _this.fatalError("connection-lost");
    });

    this.socket.on(MESSAGES.COUNTDOWN_STEP, function(step) {
      try {
        log(`Countdown step ${step}`);
        _this.renderer.countdown.step(step);
        if (step === 4) {
          _this.alertQueue.alert("RACE!!!");
          _this.renderer.game.active = true;
          _this.control(_this.keyStorage.storage);
        }
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.COUNTDOWN_STEP, error);
      }
    });

    this.socket.on(MESSAGES.GAME_IS_FULL, function() {
      log(`Game full`);
      _this.fatalError("full");
    });

    this.socket.on(MESSAGES.GAME_STARTED, function(game) {
      log(`Game Started`);
      _this.renderGame(game);
    });

    this.socket.on(MESSAGES.HURDLE_ACTIVATED, function(hurdle) {
      try {
        _this.renderer.addHurdle(hurdle);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.HURDLE_ACTIVATED, error);
      }
    });

    this.socket.on(MESSAGES.HURDLE_COLLISION, function(data) {
      try {
        _this.renderer.collidedWithHurdle(data);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.HURDLE_COLLISION, error);
      }
    });

    this.socket.on(MESSAGES.NEXT_ROUND, function(newRound) {
      try {
        _this.renderer.roundDisplay.setCurrentRound(newRound);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.NEXT_ROUND, error);
      }
    });

    this.socket.on(MESSAGES.OUT_OF_BOUNDS, function() {
      try {
        _this.alertQueue.alert("Return back to track within 2 seconds");
        _this.renderer.assets.alarm.play();
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.OUT_OF_BOUNDS, error);
      }
    });

    this.socket.on(MESSAGES.PLAYER_FINISHED, function() {
      _this.renderer.showGameEnd();
      setTimeout(() => {
        window.location.href = "/";
      }, 5000);
    });

    this.socket.on(MESSAGES.PLAYER_JOINED, function(name) {
      log(`${name} joined`);
      _this.alertQueue.alert(`${name} joined`);
    });

    this.socket.on(MESSAGES.PLAYER_LEFT, function(name) {
      log(`${name} left`);
      _this.alertQueue.alert(`${name} left`);
    });

    this.socket.on(MESSAGES.POSITION_CHANGED, function(players) {
      try {
        _this.renderer.control(players);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.POSITION_CHANGED, error);
      }
    });

    this.socket.on(MESSAGES.POWERUP_COLLECTED, function(data) {
      try {
        _this.renderer.game.powerupCollected(data.powerupId, data.playerId);
        _this.renderer.assets.collect.play();
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.POWERUP_COLLECTED, error);
      }
    });

    this.socket.on(MESSAGES.POWERUP_VISIBLE_AGAIN, function(msg) {
      try {
        _this.renderer.game.powerups[msg.powerupId].reactivate(msg.type);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.POWERUP_VISIBLE_AGAIN, error);
      }
    });

    this.socket.on(MESSAGES.RANK_CHANGED, function(newRank) {
      try {
        log(`New rank ${newRank}`);
        _this.renderer.rankingDisplay.setCurrentRank(newRank);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.RANK_CHANGED, error);
      }
    });

    this.socket.on(MESSAGES.SLOW_FIRST, function(position) {
      try {
        _this.renderer.showExplosionAt(position);
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.SLOW_FIRST, error);
      }
    });

    this.socket.on(MESSAGES.RESET_TO_CHECKPOINT, function() {
      try {
        _this.alertQueue.alert("Reset to last checkpoint");
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.RESET_TO_CHECKPOINT, error);
      }
    });

    this.socket.on(MESSAGES.TRIGGER_POWERUP, function(playerId) {
      try {
        const player = _this.renderer.game.players[playerId];
        _this.alertQueue.alert(
          `${player.name} activated ${player.equippedPowerup}`
        );
        player.triggerPowerup();
        _this.assets.activation.play();
      } catch (error) {
        _this.showErrorForEvent(MESSAGES.TRIGGER_POWERUP, error);
      }
    });

    this.socket.on(MESSAGES.WAIT_FOR_PLAYERS, function() {
      log(`Waiting for more players`);
      _this.alertQueue.alert(`Waiting for more players...`);
      visibility.show(document.getElementById("splash"));
      document.getElementById("startButton").onclick = () => {
        visibility.hide(document.getElementById("splash"));
        _this.socket.emit(MESSAGES.READY);
      };
    });
  }

  /**
   * Show error message on websocket event failure
   *
   * @param {MESSAGES} socketEventName - Name of the failed socket event
   * @param {string} error - Error message
   *
   * @returns {void}
   */
  showErrorForEvent(socketEventName, error) {
    log(`Failed to process ${socketEventName}: ${error}`);
  }
}

module.exports = Client;
