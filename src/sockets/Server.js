const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
  pingInterval: 200,
  pingTimeout: 2000
});
const MESSAGES = require("./MESSAGES");
const pkg = require("../../package.json");
const log = require("debug")("server");
const verbose = require("debug")("server_verbose");
const Game = require("../game/Game");
const Map = require("../game/map/Map");
const POWERUP_TYPES = require("../game/map/objects/POWERUP_TYPES");
const CoordinateCalculation = require("../game/map/CoordinateCalculation");
const Collision = require("../game/map/CollisionDetection");
require("canvas-5-polyfill");
require("jsdom-global")();

/**
 * Central server for the game. The root object instantiated in node.
 *
 * <a href="server.svg">
 *   <img src="server.svg" style="width: 100%;"></img>
 * </a>
 *
 * @property {number} port - tcp port
 * @property {boolean} startRanking - calculate player ranks if true
 * @property {Game} game - currently running game instance
 * @property {number} countdownInterval - setInterval id of the interval of the pre-game countdown
 * @property {number} controlInterval - setInterval id of the interval that updates the player positions
 */
class Server {
  /**
   * @constructs Server
   * @param {number} [port=3000] - port to listen on
   */
  constructor(port = 3000) {
    this.port = port;
    this.startRanking = false;
    this.newGame();

    this.setUpExpressPaths();
    this.connectSockets();
  }

  /**
   * Join a game
   *
   * @param {string} id - ID of the player who wants to join
   * @param {string} name - player name
   * @param {string} color - chosen car color
   * @param {Function} wait - wait callback, waiting for more players to start
   * @param {Function} full - full callback, the game is full
   *
   * @returns {void}
   */
  joinGame(id, name, color, wait, full) {
    try {
      this.game.join(id, name, color);
    } catch (error) {
      if (error.message === "full") {
        full();
      } else {
        throw new Error("Unknown join error: " + error);
      }
    }
    wait();
    if (this.game.isFull()) {
      this.startGame();
    }
  }

  /**
   * Start the actual race inside the game
   *
   * @fires Server#GAME_STARTED
   * @fires Server#COUNTDOWN_STEP
   *
   * @returns {void}
   */
  startGame() {
    if (this.game.active) return;
    this.game.active = true;
    setTimeout(() => {
      /**
       * Broadcast that the game started
       *
       * @event Server#GAME_STARTED
       * @type {Game}
       */
      io.sockets.emit(MESSAGES.GAME_STARTED, this.game);
    }, 500);
    let step = 1;
    let _this = this;
    this.countdownInterval = setInterval(() => {
      if (step === 4) {
        clearInterval(this.countdownInterval);
        this.controlInterval = setInterval(() => _this.control(), 40);
      }
      /**
       * Broadcast that the countdown ticked
       *
       * @event Server#COUNTDOWN_STEP
       * @type {number}
       */
      io.sockets.emit(MESSAGES.COUNTDOWN_STEP, step++);
    }, 1500);
  }

  /**
   * Leave a game
   *
   * @param {string} id - ID of the player who wants to leave
   *
   * @fires Server#PLAYER_LEFT
   *
   * @returns {void}
   */
  leaveGame(id) {
    if (this.game.players[id]) {
      /**
       * Broadcast that a player left
       *
       * @event Server#PLAYER_LEFT
       * @type {string}
       */
      io.sockets.emit(MESSAGES.PLAYER_LEFT, this.game.players[id].name);
    }
    this.game.leave(id);
    if (this.game.isEmpty()) {
      this.newGame();
    }
  }

  /**
   * Start a new game with a random map
   *
   * @returns {void}
   */
  newGame() {
    log("All players left, starting new Game");
    clearInterval(this.countdownInterval);
    clearInterval(this.controlInterval);
    this.map = Map.getRandomMap();
    this.mapCoordinates = CoordinateCalculation.calculateSmoothTrackCoordinates(
      this.map.coordinates
    );
    this.game = new Game(this.map);
    this.game.initializePowerups();
  }

  /**
   * Rank the players
   *
   * @fires Server#RANK_CHANGED
   *
   * @returns {void}
   */
  rankPlayers() {
    this.game.rankPlayers((playerId, newRank) => {
      /**
       * Singlecast new rank
       *
       * @event Server#RANK_CHANGED
       * @type {number}
       */
      io.to(playerId).emit(MESSAGES.RANK_CHANGED, newRank);
    });
  }

  /**
   * Calculate new player positions
   *
   * @fires Server#POSITION_CHANGED
   *
   * @returns {void}
   */
  control() {
    // HACK: needed so we don't broadcast the entire player
    let updateObj = {};
    for (const id in this.game.players) {
      if (this.game.players.hasOwnProperty(id)) {
        this.game.players[id].control();
        const player = this.game.players[id];

        if (player.keyStorage.isSpace()) this.triggerPowerup(player);

        this.passedCheckpoint(player);
        this.handleCarCollision(player);
        this.handlePowerupCollision(player);
        this.handleHurdleCollision(player);
        this.handleOutsideTrack(player);
        updateObj[id] = player.getPositionData();
      }
    }
    if (this.startRanking) this.rankPlayers();
    /**
     * Broadcast positions
     *
     * @event Server#POSITION_CHANGED
     * @type {object}
     */
    io.volatile.sockets.emit(MESSAGES.POSITION_CHANGED, updateObj);
  }

  /**
   * Reset player position to last passed checkpoint
   *
   * @param {object} player - The player whose position is reset
   *
   * @fires Server#RESET_TO_CHECKPOINT
   *
   * @returns {void}
   */
  resetPlayerPositionToLastCheckpoint(player) {
    player.resetPositionToLastCheckpoint(this.map.coordinates);
    /**
     * Singlecast a position reset
     *
     * @event Server#RESET_TO_CHECKPOINT
     * @type {void}
     */
    io.to(player.id).emit(MESSAGES.RESET_TO_CHECKPOINT);
  }

  /**
   * Track the checkpoints the which the user passes
   *
   * @param {Car} player - The player instance
   *
   * @fires Server#NEXT_ROUND
   * @fires Server#PLAYER_FINISHED
   *
   * @returns {void}
   */
  passedCheckpoint(player) {
    const checkpointId = Collision.detect(
      player,
      this.map.coordinates,
      (65 + 10) / 2
    );

    const isValidOnFinishLine =
      player.lastCheckpoint === this.map.coordinates.length - 1 &&
      checkpointId === 0;

    if (checkpointId !== -1) {
      if (isValidOnFinishLine) {
        // Increase number of rounds only when player drives a whole round (i.e. comes from a certain direction)
        player.nextRound();
        /**
         * Singlecast round change
         *
         * @event Server#NEXT_ROUND
         * @type {number}
         */
        io.to(player.id).emit(MESSAGES.NEXT_ROUND, player.currentRound);
        if (player.isLastRoundOver()) {
          /**
           * Broadcast that a player finished
           *
           * @event Server#PLAYER_FINISHED
           * @type {void}
           */
          io.sockets.emit(MESSAGES.PLAYER_FINISHED);
          setTimeout(() => this.leaveGame(player.id), 5000);
        }
      }
      if (checkpointId === player.lastCheckpoint + 1 || isValidOnFinishLine) {
        // Update checkpoint index
        player.lastCheckpoint = checkpointId;
      } else if (checkpointId - player.lastCheckpoint !== 0) {
        // User skipped a checkpoint, reset to last checkpoint
        this.resetPlayerPositionToLastCheckpoint(player);
      }
    }
  }

  /**
   * Reset the car to the last checkpoint if it doesn't return to the track after 2 seconds
   *
   * @param {Car} player - The player instance
   *
   * @fires Server#OUT_OF_BOUNDS
   *
   * @returns {void}
   */
  handleOutsideTrack(player) {
    if (this.isOutsideTrack(player) && !player.collided) {
      player.collided = true;
      player.boost = false;
      /**
       * Singlecast that a player is out of bounds
       *
       * @event Server#OUT_OF_BOUNDS
       * @type {void}
       */
      io.to(player.id).emit(MESSAGES.OUT_OF_BOUNDS);
      setTimeout(() => {
        player.collided = false;
        if (this.isOutsideTrack(player)) {
          this.resetPlayerPositionToLastCheckpoint(player);
        }
      }, 2000);
    }
  }

  /**
   * Detect collisions with walls
   *
   * @param {Car} player - The player instance
   *
   * @returns {boolean} - Collided or not
   */
  isOutsideTrack(player) {
    const returnedIndex = Collision.detect(player, this.mapCoordinates, 65 / 2);
    if (returnedIndex !== -1) {
      if (player.currentRound === 0 && returnedIndex === 0) {
        this.startRanking = true;
        player.currentRound = 1;
      }
      player.lastCoordinateHitbox = returnedIndex;
      return false;
    } else {
      return true;
    }
  }

  /**
   * Slow down player who is involved in a collision with another car
   *
   * @param {Car} player - The involved player
   *
   * @returns {void}
   */
  handleCarCollision(player) {
    const playerArray = [];
    for (const id in this.game.players) {
      if (this.game.players.hasOwnProperty(id)) {
        if (player.id !== id) {
          playerArray.push(this.game.players[id].position);
        }
      }
    }

    const didHitOtherCar = Collision.detect(player, playerArray, 15) !== -1;

    if (didHitOtherCar) {
      player.onCarCollision();
    }
  }

  /**
   * Player collects a Powerup when driving above
   *
   * @param {Car} player - The player instance
   *
   * @fires Server#POWERUP_COLLECTED
   * @fires Server#POWERUP_VISIBLE_AGAIN
   *
   * @returns {void}
   */
  handlePowerupCollision(player) {
    if (player.equippedPowerup) {
      return;
    }
    const pickedPowerupId = Collision.detect(
      player,
      this.game.powerups,
      10,
      true
    );
    if (pickedPowerupId !== -1 && this.game.powerups[pickedPowerupId].enabled) {
      const pickerPowerup = this.game.powerups[pickedPowerupId];

      this.game.powerupCollected(pickedPowerupId, player.id);
      /**
       * Broadcast that a powerup has been collected
       *
       * @event Server#POWERUP_COLLECTED
       * @type {object}
       * @property {number} powerupId - id of the collected powerup
       * @property {string} playerId - id of the collecting player
       */
      io.sockets.emit(MESSAGES.POWERUP_COLLECTED, {
        powerupId: pickedPowerupId,
        playerId: player.id
      });

      setTimeout(() => {
        pickerPowerup.reactivate();
        /**
         * Broadcast that a powerup has been collected
         *
         * @event Server#POWERUP_VISIBLE_AGAIN
         * @type {object}
         * @property {number} powerupId - id of the collected powerup
         * @property {string} type - new powerup type
         */
        io.sockets.emit(MESSAGES.POWERUP_VISIBLE_AGAIN, {
          powerupId: pickedPowerupId,
          type: pickerPowerup.type
        });
      }, 5000);
    }
  }

  /**
   * Player's speed is set to 0 when the collided with a hurdle not placed by himself
   *
   * @param {Car} player - The player instance
   *
   * @fires Server#HURDLE_COLLISION
   *
   * @returns {void}
   */
  handleHurdleCollision(player) {
    this.game.didPlayerCollideWithHurdle(player, hitHurdleId => {
      player.speed *= -0.5;
      /**
       * Broadcast that a player has collided with a hurdle
       *
       * @event Server#HURDLE_COLLISION
       * @type {object}
       * @property {number} hurdleId - id of the hurdle
       * @property {string} playerId - id of the colliding player
       */
      io.sockets.emit(MESSAGES.HURDLE_COLLISION, {
        hurdleId: hitHurdleId,
        playerId: player.id
      });
    });
  }

  /**
   * Handle the powerup activation for a player
   *
   * @param {Car} player - The player who activated the powerup
   *
   * @fires Server#HURDLE_ACTIVATED
   * @fires Server#SLOW_FIRST
   * @fires Server#TRIGGER_POWERUP
   *
   * @returns {void}
   */
  triggerPowerup(player) {
    if (!player.equippedPowerup) return;
    log(`${player.name} activated ${player.equippedPowerup}`);
    switch (player.equippedPowerup) {
      case POWERUP_TYPES.BOOST: {
        player.activateBoost();
        break;
      }
      case POWERUP_TYPES.HURDLE: {
        const hurdle = this.game.placeHurdle(player.position, player.id);
        /**
         * Broadcast that a hurdle has been activated
         *
         * @event Server#HURDLE_ACTIVATED
         * @type {Hurdle}
         */
        io.sockets.emit(MESSAGES.HURDLE_ACTIVATED, hurdle);
        break;
      }
      case POWERUP_TYPES.SLOW_FIRST: {
        const firstPlayer = this.game.findFirstPlayer();
        if (firstPlayer.id !== player.id) {
          firstPlayer.slowFirst();
          /**
           * Broadcast that slow first has been activated
           *
           * @event Server#SLOW_FIRST
           * @type {Coordinates}
           */
          io.sockets.emit(MESSAGES.SLOW_FIRST, firstPlayer.position);
        }
        break;
      }
    }
    player.equippedPowerup = false;
    /**
     * Broadcast that a player has activated a powerup
     *
     * @event Server#TRIGGER_POWERUP
     * @type {string}
     */
    io.sockets.emit(MESSAGES.TRIGGER_POWERUP, player.id);
  }

  /**
   * Set up express paths and redirects
   *
   * @returns {void}
   */
  setUpExpressPaths() {
    app.use(express.static("src/html"));
    app.use(express.static("src/assets"));
    app.use(express.static("out"));
    app.use(express.static("coverage"));
    app.get("/docs", function(req, res) {
      res.redirect(302, "/docs/" + pkg.name + "/" + pkg.version);
    });
    app.get("/coverage", function(req, res) {
      res.redirect(302, "/lcov-report");
    });
    app.get("/source", function(req, res) {
      res.redirect(302, pkg.homepage);
    });
  }

  /**
   * Create new server
   *
   * @returns {void}
   *
   * @listens Client#CONTROL
   *
   * @fires Server#WAIT_FOR_PLAYERS
   * @fires Server#PLAYER_JOINED
   * @fires Server#GAME_IS_FULL
   */
  connectSockets() {
    let port = this.port;
    http.listen(port, function() {
      log(`Now listening on port ${port}`);
    });

    let _this = this;
    io.on("connection", function(socket) {
      log(`User ${socket.id} connected`);

      socket.on(MESSAGES.CONTROL, function(keys) {
        try {
          _this.game.players[socket.id].keyStorage.storage = keys;
          verbose(`Control message received from ${socket.id}`);
        } catch (e) {
          console.error(e);
        }
      });

      socket.on(MESSAGES.JOIN_GAME, function(msg) {
        try {
          _this.joinGame(
            socket.id,
            msg.name,
            msg.color,
            () => {
              /**
               * the game is full, refuse a client to join
               *
               * @event Server#WAIT_FOR_PLAYERS
               * @type {void}
               */
              io.to(socket.id).emit(MESSAGES.WAIT_FOR_PLAYERS);
              /**
               * the game is full, refuse a client to join
               *
               * @event Server#PLAYER_JOINED
               * @type {string}
               */
              io.sockets.emit(MESSAGES.PLAYER_JOINED, msg.name);
            },
            () => {
              /**
               * the game is full, refuse a client to join
               *
               * @event Server#GAME_IS_FULL
               * @type {void}
               */
              io.to(socket.id).emit(MESSAGES.GAME_IS_FULL);
            }
          );
          log(
            `User "${msg.name}" (${socket.id}) wants to join as ${msg.color}`
          );
        } catch (e) {
          console.error(e);
        }
      });

      socket.on(MESSAGES.LEAVE_GAME, function() {
        try {
          _this.leaveGame(socket.id);
          log(`User ${socket.id} wants to leave`);
        } catch (e) {
          console.error(e);
        }
      });

      socket.on(MESSAGES.READY, function() {
        _this.startGame();
      });

      socket.on("disconnect", function() {
        log(`User ${socket.id} disconnected`);
      });
    });
  }

  /**
   * Close server and all client connections
   *
   * @returns {void}
   */
  disconnect() {
    log(`Stopped listening on port ${this.port}`);
    io.close();
  }
}

module.exports = Server;
