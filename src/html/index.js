const utils = require("../libs/utils");

const urlParameters = utils.getUrlParameters();

const title = document.getElementById("title");
const text = document.getElementById("text");
const joinButton = document.getElementById("joinButton");

if (urlParameters.error) {
  joinButton.innerHTML = "Reconnect";
  switch (urlParameters.error) {
    case "full":
      title.innerHTML = "Game is full :/";
      text.innerHTML = "Sorry, the game is full... Please come back later.";
      break;
    case "connection-lost":
      title.innerHTML = "Connection lost :(";
      text.innerHTML =
        "Connection to the server was lost... Please try again later.";
      break;
    default:
      title.innerHTML = "Bonk! Something funky happened...";
      text.innerHTML = `Sorry about that, an unknown error ocurred. Please come back later.<br>Error: ${urlParameters.error}`;
      break;
  }
}

joinButton.onclick = () => {
  // TODO rooms can later be implemented as url parameters
  window.location.href = "./play.html";
};
