const AssetLoader = require("../libs/AssetLoader");
const Client = require("../sockets/Client");

var client;

AssetLoader.loadAssets([
  {
    name: "blue",
    url: "/images/blue.png",
    type: "image"
  },
  {
    name: "orange",
    url: "/images/orange.png",
    type: "image"
  },
  {
    name: "yellow",
    url: "/images/yellow.png",
    type: "image"
  },
  {
    name: "black",
    url: "/images/black.png",
    type: "image"
  },
  {
    name: "items",
    url: "/images/items.png",
    type: "image"
  },
  {
    name: "bombs",
    url: "/images/bombs.png",
    type: "image"
  },
  {
    name: "medaillen",
    url: "/images/medaillen.png",
    type: "image"
  },
  {
    name: "coins",
    url: "/images/coins.png",
    type: "image"
  },
  {
    name: "finishline",
    url: "/images/finishline.png",
    type: "image"
  },
  {
    name: "racingBackground",
    url: "/images/racingBackground.png",
    type: "image"
  },
  {
    name: "trafficlight1",
    url: "/images/countdown1.png",
    type: "image"
  },
  {
    name: "trafficlight2",
    url: "/images/countdown2.png",
    type: "image"
  },
  {
    name: "trafficlight3",
    url: "/images/countdown3.png",
    type: "image"
  },
  {
    name: "trafficlight4",
    url: "/images/countdown4.png",
    type: "image"
  },
  {
    name: "explosion",
    url: "/images/explosion.png",
    type: "image"
  },
  {
    name: "alarm",
    url: "/sounds/alarm.wav",
    type: "audio"
  },
  {
    name: "explosionSound",
    url: "/sounds/explosionSound.wav",
    type: "audio"
  },
  {
    name: "finishSound",
    url: "/sounds/finishSound.wav",
    type: "audio"
  },
  {
    name: "activation",
    url: "/sounds/activation.wav",
    type: "audio"
  },
  {
    name: "collect",
    url: "/sounds/coin.wav",
    type: "audio"
  },
  {
    name: "engine",
    url: "/sounds/engine.wav",
    type: "audio"
  },
  {
    name: "countdownSoundStart",
    url: "/sounds/countdownstart.wav",
    type: "audio"
  },
  {
    name: "countdownSoundEnd",
    url: "/sounds/countdownend.wav",
    type: "audio"
  }
]).then(assets => {
  client = new Client(assets);
});
