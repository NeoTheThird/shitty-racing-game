# SEP-Gruppe-NN

Created as part of the javascript practical course at university munich during the winter semester of 2019. The assignment was to create a real-time competitive multiplayer game using vanilla javascript and HTML5 and a server built on socketIO. The result does not try to be production ready, but we decided to release it anyways under the terms of the GPL license in hopes it might be useful to someone in the future.

## Set up for development

First of all, you'll want to have [node](https://nodejs.org) and [npm](https://npmjs.com) installed. Navigate to your local working copy of the repository and run `npm install` to install dev dependencies.

## Start test server

You can start the server on port 3000 by running `npm start`. Open [http://localhost:3000/](http://localhost:3000/) in your browser to play. The game uses localStorage to persist your ID, so make sure to use sandboxed tabs for testing on a single machine.

## Workflow

The workflow is documented in [CONTRIBUTING.md](./CONTRIBUTING.md).

## Dependency analysis

This project aims to use as little production-dependencies as possible. All graphics functionality is implemented in pure HTML5 and JavaScript.

The server is built using [express](https://www.npmjs.com/package/express) and [socket.io](https://www.npmjs.com/package/socket.io)/[socket.io-client](https://www.npmjs.com/package/socket.io-client). Additionally, [uws](https://www.npmjs.com/package/uws) - a dependency of socketIO - is pinned to a newer version to work around an upstream bug.

To use the JavaScript DOM API in NodeJS, [jsdom](https://www.npmjs.com/package/jsdom), [jsdom-global](https://www.npmjs.com/package/jsdom-global), [canvas](https://www.npmjs.com/package/canvas) and [canvas-5-polyfill](https://www.npmjs.com/package/canvas-5-polyfill) are used.

[Browserify](https://www.npmjs.com/package/browserify) is run on the client-side JavaScript files before serving to enable node-style `require` imports in the browser.

The only trivial depenency is the name-spaced logging-library [debug](https://www.npmjs.com/package/debug). Since it interfaces nicely with internal logging of various JavaScript engines, it has some merit to use it rather than re-implementing name-spaced logging.

## Copyright and License

This Game is licensed under the GNU General Public License version 3 or any later version.

Copyright (c) 2019-2020 Niklas Amslgruber, Marco Asbeck, Güner Bölükbasi and Jan Sprinz.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [www.gnu.org/licenses](http://www.gnu.org/licenses/).