"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Game = require("../../../src/game/Game");

const proxyquire = require("proxyquire").noCallThru();
const expect = chai.expect;
chai.use(sinonChai);

const basicMapModel = {
  startingPositions: [1, 2],
  powerupPositions: [{ x: 1, y: 2 }]
};

describe("game/Game.js", function() {
  describe("constructor()", function() {
    it("should construct game", function() {
      expect(new Game("i'm a map").map).to.eql("i'm a map");
    });
  });
  describe("powerupCollected()", function() {
    it("should give player powerup", function() {
      const game = new Game({ powerupPositions: [{ x: 1, y: 2 }] });
      game.powerups = [{ type: "foo" }];
      game.players = { a: { equippedPowerup: undefined } };
      game.powerupCollected(0, "a");
    });
  });
  describe("join()", function() {
    it("should let players join", function() {
      const game = new Game(basicMapModel);
      expect(Object.keys(game.players).length).to.eql(0);
      expect(game.join("a")).to.have.returned;
      expect(Object.keys(game.players).length).to.eql(1);
      expect(game.join("b")).to.have.returned;
      expect(Object.keys(game.players).length).to.eql(2);
    });
    it("should throw when game active", function() {
      const game = new Game(basicMapModel);
      expect(game.isEmpty()).to.eql(true);
      game.join("a");
      game.active = true;
      expect(Object.keys(game.players).length).to.eql(1);
      expect(game.isEmpty()).to.eql(false);
      expect(game.isFull()).to.eql(false);
      try {
        game.join("c");
      } catch (e) {
        expect(e.message).to.eql("full");
        expect(Object.keys(game.players).length).to.eql(1);
        return true;
      }
      throw new Error("Should have thrown when active");
    });
    it("should throw when full", function() {
      const game = new Game(basicMapModel);
      game.join("a");
      game.join("b");
      expect(Object.keys(game.players).length).to.eql(2);
      try {
        game.join("c");
      } catch (e) {
        expect(e.message).to.eql("full");
        expect(Object.keys(game.players).length).to.eql(2);
        return true;
      }
      throw new Error("Should have thrown when full");
    });
    it("should pass when already connected", function() {
      const game = new Game(basicMapModel);
      expect(Object.keys(game.players).length).to.eql(0);
      game.join("a");
      expect(Object.keys(game.players).length).to.eql(1);
      game.join("a");
      expect(Object.keys(game.players).length).to.eql(1);
    });
  });
  describe("leave()", function() {
    it("should leave", function() {
      const game = new Game(basicMapModel);
      game.join("a");
      game.join("b");
      expect(Object.keys(game.players).length).to.eql(2);
      expect(game.leave("a")).to.have.returned;
      expect(Object.keys(game.players).length).to.eql(1);
    });
    it("should pass on invalid id", function() {
      const game = new Game(basicMapModel);
      game.join("a");
      game.join("b");
      expect(Object.keys(game.players).length).to.eql(2);
      expect(game.leave("c")).to.have.returned;
      expect(Object.keys(game.players).length).to.eql(2);
    });
  });
  describe("initializePowerups()", function() {
    it("should create powerups", function() {
      const game = new Game(basicMapModel);
      game.powerups = [];
      expect(game.powerups.length).to.eql(0);
      game.initializePowerups();
      expect(game.powerups.length).to.eql(1);
    });
  });
  describe("rankPlayers()", function() {
    it("should callback rank changes and set ranks", function() {
      const onRankChangedSpy = sinon.spy();
      const game = new Game(basicMapModel);
      game.players = {
        a: {
          lastCoordinateHitbox: 1,
          currentRound: 1,
          rank: 0
        },
        b: {
          lastCoordinateHitbox: 1,
          currentRound: 2,
          rank: 2
        }
      };
      game.rankPlayers(onRankChangedSpy);
      expect(onRankChangedSpy).to.have.been.calledWith("a", 1);
      expect(game.players.a.rank).to.eql(1);
      expect(onRankChangedSpy).to.have.been.calledWith("b", 0);
      expect(game.players.b.rank).to.eql(0);

      game.players.a.lastCoordinateHitbox = 1337;
      game.players.a.currentRound = 2;
      game.players.b.lastCoordinateHitbox = 1;
      game.players.b.currentRound = 2;
      game.rankPlayers(onRankChangedSpy);
      expect(onRankChangedSpy).to.have.been.calledWith("a", 0);
      expect(game.players.a.rank).to.eql(0);
      expect(onRankChangedSpy).to.have.been.calledWith("b", 1);
      expect(game.players.b.rank).to.eql(1);

      game.players.a.lastCoordinateHitbox = 1337;
      game.players.b.lastCoordinateHitbox = 1337;
      game.rankPlayers(onRankChangedSpy);
      expect(game.players.a.rank).to.eql(1);
      expect(game.players.b.rank).to.eql(0);

      game.players.a.currentRound = 1337;
      game.rankPlayers(onRankChangedSpy);
      expect(game.players.a.rank).to.eql(0);
      expect(game.players.b.rank).to.eql(1);
    });
  });
  describe("findFirstPlayer()", function() {
    it("should return first player", function() {
      const game = new Game(basicMapModel);
      game.players = {
        a: {
          rank: 42
        },
        b: {
          someOther: "property",
          rank: 0
        }
      };
      expect(game.findFirstPlayer()).to.eql({
        someOther: "property",
        rank: 0
      });
    });
  });
  describe("didPlayerCollideWithHurdle()", function() {
    it("should call back if there was a collision with a foreign hurdle", function() {
      const onCollisionSpy = sinon.spy();
      const detectFake = sinon.fake.returns(0);
      const _Game = proxyquire("../../../src/game/Game", {
        "./map/CollisionDetection": {
          detect: detectFake
        }
      });
      const game = new _Game(basicMapModel);
      game.hurdles = [{ placedBy: "wasd" }];
      const playerStub = {
        id: "asdf"
      };
      game.didPlayerCollideWithHurdle(playerStub, onCollisionSpy);
      expect(onCollisionSpy).to.have.been.calledWith(0);
    });
    it("should not call back if there was no collision", function() {
      const onCollisionSpy = sinon.spy();
      const detectFake = sinon.fake.returns(-1);
      const _Game = proxyquire("../../../src/game/Game", {
        "./map/CollisionDetection": {
          detect: detectFake
        }
      });
      const game = new _Game(basicMapModel);
      const playerStub = {
        id: "asdf"
      };
      game.didPlayerCollideWithHurdle(playerStub, onCollisionSpy);
      expect(onCollisionSpy).to.not.have.been.called;
    });
    it("should not call back if there was a collision with an own hurdle", function() {
      const onCollisionSpy = sinon.spy();
      const detectFake = sinon.fake.returns(0);
      const _Game = proxyquire("../../../src/game/Game", {
        "./map/CollisionDetection": {
          detect: detectFake
        }
      });
      const game = new _Game(basicMapModel);
      game.hurdles = [{ placedBy: "asdf" }];
      const playerStub = {
        id: "asdf"
      };
      game.didPlayerCollideWithHurdle(playerStub, onCollisionSpy);
      expect(onCollisionSpy).to.not.have.been.called;
    });
  });
  describe("placeHurdle()", function() {
    it("should place hurdle", function() {
      const _Game = proxyquire("../../../src/game/Game", {
        "./map/objects/Hurdle": class {
          constructor(position, placedBy) {
            this.position = position;
            this.placedBy = placedBy;
          }
        }
      });
      const game = new _Game(basicMapModel);
      expect(game.placeHurdle({ x: 0, y: 1 }, "someone")).to.eql({
        position: { x: 0, y: 1 },
        placedBy: "someone"
      });
      expect(game.hurdles).to.eql([
        {
          position: { x: 0, y: 1 },
          placedBy: "someone"
        }
      ]);
    });
  });
});
