"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Car = require("../../../../../src/game/map/objects/Car");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/objects/Car.js", function() {
  describe("constructor()", function() {
    it("should construct car", function() {
      var jsdom = require("jsdom-global")();
      document.body.innerHTML = "hello";
      var contextFake = {};
      var car = new Car(1, { x: 100, y: 20 }, "User1", "orange", contextFake, {
        autos: "autos.svg"
      });
      expect(car).to.exist;
    });
    it("should add event listeners");
  });
  describe("draw()", function() {
    it("should draw", function() {
      var jsdom = require("jsdom-global")();
      document.body.innerHTML = "hello";
      var contextFake = {
        drawImage: sinon.spy(),
        setTransform: sinon.spy()
      };
      var car = new Car(1, { x: 100, y: 20 }, "User1", "orange", contextFake, {
        autos: "autos.svg"
      });
      expect(car.draw()).to.have.returned;
      expect(contextFake.setTransform).to.have.been.calledWith(
        1,
        0,
        -0,
        1,
        100,
        20
      );
      expect(contextFake.setTransform).to.have.been.calledWith(
        1,
        0,
        0,
        1,
        0,
        0
      );
      expect(contextFake.drawImage).to.have.been.calledOnce;
    });
  });
  describe("activateBoost()", function() {
    it("should activate boost", function(done) {
      const car = new Car();
      expect(car.boost).to.eql(false);
      car.activateBoost(10);
      expect(car.boost).to.eql(true);
      setTimeout(() => {
        expect(car.boost).to.eql(false);
        done();
      }, 10);
    });
    it("should keep boost if called twice", function(done) {
      const car = new Car();
      expect(car.boost).to.eql(false);
      car.activateBoost(10);
      expect(car.boost).to.eql(true);
      car.activateBoost(20);
      expect(car.boost).to.eql(true);
      setTimeout(() => {
        expect(car.boost).to.eql(true);
        setTimeout(() => {
          expect(car.boost).to.eql(false);
          done();
        }, 10);
      }, 10);
    });
  });
  describe("blockSteering()", function() {
    it("should block steering", function(done) {
      const car = new Car();
      expect(car.enabled).to.eql(true);
      car.blockSteering(10);
      expect(car.enabled).to.eql(false);
      setTimeout(() => {
        expect(car.enabled).to.eql(true);
        done();
      }, 10);
    });
    it("should keep block -1", function() {
      const car = new Car();
      expect(car.enabled).to.eql(true);
      car.blockSteering(-1);
      expect(car.enabled).to.eql(false);
    });
    it("should keep block if called twice", function(done) {
      const car = new Car();
      expect(car.enabled).to.eql(true);
      car.blockSteering(10);
      expect(car.enabled).to.eql(false);
      car.blockSteering(20);
      expect(car.enabled).to.eql(false);
      setTimeout(() => {
        expect(car.enabled).to.eql(false);
        setTimeout(() => {
          expect(car.enabled).to.eql(true);
          done();
        }, 10);
      }, 10);
    });
  });
  describe("unblockSteering", function() {
    it("should unblock", function() {
      const car = new Car();
      expect(car.enabled).to.eql(true);
      car.unblockSteering();
      expect(car.enabled).to.eql(true);
    });
    it("should clearTimeout", function(done) {
      const car = new Car();
      car.blockTimeout = setTimeout(() => {
        done("this should not be called");
      }, 500);
      expect(car.enabled).to.eql(true);
      car.unblockSteering();
      expect(car.enabled).to.eql(true);
      done();
    });
  });
});
