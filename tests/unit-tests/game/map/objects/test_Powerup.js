"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Powerup = require("../../../../../src/game/map/objects/Powerup");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/objects/Powerup.js", function() {
  describe("constructor()", function() {
    it("should create random powerup", function() {
      expect(new Powerup()).to.exist;
    });
  });
});
