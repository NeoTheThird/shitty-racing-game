"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const MapObject = require("../../../../../src/game/map/objects/MapObject");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/objects/MapObject.js", function() {
  describe("update()", function() {
    it("should update step", function() {
      const p = new MapObject();
      expect(p.currentAnimationStep).to.eql(0);
      p.update(1337);
      expect(p.currentAnimationStep).to.eql(1);
    });
  });
  describe("draw()", function() {
    it("should draw", function() {
      const contextFake = {
        drawImage: sinon.spy(),
        setTransform: sinon.spy()
      };
      const p = new MapObject(
        { x: 0, y: 1 },
        contextFake,
        "foo",
        0,
        1,
        777,
        777
      );
      p.draw();
      expect(contextFake.drawImage).to.have.been.called;
    });
  });
});
