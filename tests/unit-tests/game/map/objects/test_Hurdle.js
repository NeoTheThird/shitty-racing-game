"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Hurdle = require("../../../../../src/game/map/objects/Hurdle");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/objects/Hurdle.js", function() {
  describe("constructor()", function() {
    it("should create hurdle", function() {
      expect(new Hurdle({ x: 0, y: 1 }, 666, "context", "texture")).to.exist;
    });
  });
});
