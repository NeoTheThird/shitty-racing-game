"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Map = require("../../../../src/game/map/Map");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/Map.js", function() {
  describe("constructor()", function() {
    it("should create map object", function() {
      var lineToSpy = sinon.spy();
      global.Path2D = class {
        constructor() {}
        lineTo(x, y) {
          lineToSpy(x, y);
        }
      };
      const map = new Map({
        name: "my name",
        startingPositions: ["foo", "bar"],
        coordinates: [
          { x: 1, y: 2 },
          { x: 10, y: 10 }
        ]
      });
      expect(map.name).to.eql("my name");
      expect(map.coordinates).to.eql([
        { x: 1, y: 2 },
        { x: 10, y: 10 }
      ]);
      expect(map.startingPositions).to.eql(["foo", "bar"]);
    }).timeout(5000);
  });
  describe("drawMap()", function() {
    it("should draw map", function() {
      global.Path2D = class {
        constructor() {}
        lineTo() {}
      };
      var contextFake = {
        putImageData: sinon.spy()
      };
      const map = new Map(
        {
          name: "my name",
          startingPositions: ["foo", "bar"],
          coordinates: [
            { x: 1, y: 2 },
            { x: 10, y: 10 }
          ]
        },
        contextFake,
        contextFake
      );
      expect(map.drawMap()).to.have.returned;
      expect(contextFake.putImageData).to.have.been.calledWith(map.mapImage);
    });
  });
});
