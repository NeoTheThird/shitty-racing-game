"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Coordinates = require("../../../../src/game/map/Coordinates");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/Coordinates.js", function() {
  describe("constructor()", function() {
    it("should create new object", function() {
      const coordinates = new Coordinates();
      expect(coordinates).to.eql({
        x: 0,
        y: 0,
        angle: 0,
        angle_sin: 0,
        angle_cos: 1
      });
    });
    it("should complete given object", function() {
      const coordinates = new Coordinates({ x: 1, y: 2 });
      expect(coordinates).to.eql({
        x: 1,
        y: 2,
        angle: 0,
        angle_sin: 0,
        angle_cos: 1
      });
    });
    it("should calculate sin and cos", function() {
      const coordinates = new Coordinates({ x: 1, y: 2, angle: Math.PI });
      expect(coordinates).to.eql({
        x: 1,
        y: 2,
        angle: Math.PI,
        angle_sin: 1.2246467991473532e-16,
        angle_cos: -1
      });
    });
  });
});
