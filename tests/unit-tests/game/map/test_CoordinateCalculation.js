"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const CoordinateCalculation = require("../../../../src/game/map/CoordinateCalculation");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/map/CoordinateCalculation.js", function() {
  describe("calculateSmoothTrackCoordinates()", function() {
    it("should return smoothed track", function() {
      expect(
        CoordinateCalculation.calculateSmoothTrackCoordinates([
          { x: 10, y: 33 },
          { x: 777, y: 50 },
          { x: 9, y: 0 }
        ])
      ).to.not.eql(null);
    });
  });
  describe("getCardinals()", function() {
    [
      {
        arg: 1,
        ret: [
          [1, 0, 0, 0],
          [0, 1, 0, 0]
        ]
      },
      {
        arg: 2,
        ret: [
          [1, 0, 0, 0],
          [0.5, 0.5, 0.125, -0.125],
          [0, 1, 0, 0]
        ]
      }
    ].forEach(v => {
      it("should return cardinal splines for " + v.arg, function() {
        expect(CoordinateCalculation.getCardinals(v.arg)).to.eql(v.ret);
      });
    });
  });
  describe("calculateOutlines()", function() {
    it("should return correct outlines", function() {
      expect(
        CoordinateCalculation.calculateOutlines([
          { x: 10, y: 33 },
          { x: 777, y: 50 },
          { x: 9, y: 0 }
        ])
      ).to.eql({ x: 977, y: 250 });
    });
  });
  describe("drawStreetLayer()", function() {
    it("should return cardinal splines for given intersection", function() {
      const contextSpy = {
        setLineDash: sinon.spy(),
        stroke: sinon.spy()
      };
      CoordinateCalculation.drawStreetLayer(
        contextSpy,
        "asdf",
        123,
        "foo",
        "bar",
        "baz"
      );
      expect(contextSpy.setLineDash).to.have.been.calledWith("baz");
      expect(contextSpy.stroke).to.have.been.calledWith("foo");
      expect(contextSpy.strokeStyle).to.eql("asdf");
      expect(contextSpy.lineWidth).to.eql(123);
      expect(contextSpy.lineCap).to.eql("bar");
    });
    it("should use defaults for color and cap", function() {
      const contextSpy = {
        setLineDash: sinon.spy(),
        stroke: () => {}
      };
      CoordinateCalculation.drawStreetLayer(contextSpy);
      expect(contextSpy.setLineDash).to.have.been.calledWith([]);
      expect(contextSpy.lineCap).to.eql("round");
    });
  });
});
