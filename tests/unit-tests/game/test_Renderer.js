"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Renderer = require("../../../src/game/Renderer");
const Coordinates = require("../../../src/game/map/Coordinates");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/Renderer.js", function() {
  describe("clusterfuck", function() {
    it("should create renderer", function() {
      global.window = { addEventListener: sinon.fake() };
      global.Image = class {
        constructor() {}
        addEventListener(e, cb) {
          cb();
        }
      };
      global.Audio = class {
        constructor() {}
        addEventListener(e, cb) {
          cb();
        }
      };
      global.Path2D = class {
        constructor() {}
        lineTo() {}
        arc() {}
        closePath() {}
      };
      const gameFake = {
        map: {
          name: "map name",
          coordinates: [{ x: 1, y: 1 }],
          finishLinePosition: { x: 1, y: 1 }
        },
        powerups: [{}],
        players: {
          a: {
            color: "orange",
            position: new Coordinates({ x: 0, y: 0, angle: 0 }),
            renderPosition: new Coordinates({ x: 0, y: 0, angle: 0 })
          }
        },
        hurdles: [{}],
        finishLine: [{}]
      };
      global.requestAnimationFrame = sinon.fake(() => 1);
      global.cancelAnimationFrame = sinon.spy();
      global.document = {
        createElement: () => htmlelelementfake,
        documentElement: () => htmlelelementfake
      };
      const contextFake = {
        putImageData: sinon.fake(),
        drawImage: sinon.fake(),
        clearRect: sinon.fake(),
        fillRect: sinon.fake(),
        setTransform: sinon.fake(),
        beginPath: sinon.fake(),
        closePath: sinon.fake(),
        fill: sinon.fake(),
        stroke: sinon.fake(),
        moveTo: sinon.fake(),
        lineTo: sinon.fake(),
        getImageData: sinon.fake(),
        setLineDash: sinon.fake(),
        arc: sinon.fake()
      };
      const htmlelelementfake = {
        getContext: sinon.fake(() => contextFake),
        classList: {
          remove: () => {}
        },
        querySelector: sinon.fake(() => htmlelelementfake),
        play: () => {},
        style: {},
        parentNode: {
          removeChild: () => {}
        }
      };
      const renderer = new Renderer(
        gameFake,
        "a",
        {
          engine: htmlelelementfake,
          countdownSoundStart: htmlelelementfake,
          countdownSoundEnd: htmlelelementfake
        },
        htmlelelementfake,
        htmlelelementfake,
        htmlelelementfake,
        htmlelelementfake,
        htmlelelementfake,
        htmlelelementfake,
        htmlelelementfake
      );
      renderer.stop();
      expect(global.cancelAnimationFrame).to.have.been.calledWith(1);
    });
  });
});
