"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const visibility = require("../../../../src/game/ui/visibility");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/ui/visibility.js", function() {
  describe("show()", function() {
    it("should add hidden class", function() {
      const fakeElement = {
        classList: {
          add: sinon.spy(),
          remove: sinon.spy(),
          toggle: sinon.spy()
        }
      };
      visibility.show(fakeElement);
      expect(fakeElement.classList.remove).to.have.been.calledWith("hidden");
      expect(fakeElement.classList.add).to.not.have.been.called;
      expect(fakeElement.classList.toggle).to.not.have.been.called;
    });
  });
  describe("hide()", function() {
    it("should add hidden class", function() {
      const fakeElement = {
        classList: {
          add: sinon.spy(),
          remove: sinon.spy(),
          toggle: sinon.spy()
        }
      };
      visibility.hide(fakeElement);
      expect(fakeElement.classList.remove).to.not.have.been.called;
      expect(fakeElement.classList.add).to.have.been.calledWith("hidden");
      expect(fakeElement.classList.toggle).to.not.have.been.called;
    });
  });
  describe("toggle()", function() {
    it("should toggle hidden class", function() {
      const fakeElement = {
        classList: {
          add: sinon.spy(),
          remove: sinon.spy(),
          toggle: sinon.spy()
        }
      };
      visibility.toggle(fakeElement);
      expect(fakeElement.classList.remove).to.not.have.been.called;
      expect(fakeElement.classList.add).to.not.have.been.called;
      expect(fakeElement.classList.toggle).to.have.been.calledWith("hidden");
    });
  });
});
