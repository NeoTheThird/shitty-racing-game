"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const Countdown = require("../../../../src/game/ui/Countdown");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/ui/Countdown.js", function() {
  describe("constructor", function() {
    it("should construct", function() {
      expect(new Countdown()).to.exist;
    });
  });
  describe("alert()", function() {
    it("should create alert", function(done) {
      const startSoundSpy = {
        play: sinon.spy()
      };
      const endSoundSpy = {
        play: sinon.spy()
      };
      const imgFake = {
        style: {
          display: "none"
        },
        src: false,
        parentNode: {
          removeChild: () => done()
        }
      };
      const c = new Countdown(imgFake, startSoundSpy, endSoundSpy);
      c.step(1);
      expect(startSoundSpy.play).to.have.been.calledOnce;
      expect(startSoundSpy.play).to.not.have.been.calledTwice;
      expect(endSoundSpy.play).to.not.have.been.called;
      expect(imgFake.style.display).to.eql("block");
      expect(imgFake.src).to.eql("images/countdown1.png");
      c.step(2);
      expect(startSoundSpy.play).to.have.been.calledTwice;
      expect(startSoundSpy.play).to.not.have.been.calledThrice;
      expect(endSoundSpy.play).to.not.have.been.called;
      expect(imgFake.style.display).to.eql("block");
      expect(imgFake.src).to.eql("images/countdown2.png");
      c.step(3);
      expect(startSoundSpy.play).to.have.been.calledThrice;
      expect(endSoundSpy.play).to.not.have.been.called;
      expect(imgFake.style.display).to.eql("block");
      expect(imgFake.src).to.eql("images/countdown3.png");
      c.step(4, 1);
      expect(startSoundSpy.play).to.have.been.calledThrice;
      expect(endSoundSpy.play).to.have.been.calledOnce;
      expect(endSoundSpy.play).to.not.have.been.calledTwice;
      expect(imgFake.style.display).to.eql("block");
      expect(imgFake.src).to.eql("images/countdown4.png");
    });
  });
});
