"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const AssetLoader = require("../../../src/libs/AssetLoader");

const expect = chai.expect;
chai.use(sinonChai);

describe("libs/AssetLoader.js", function() {
  describe("loadAssets()", function() {
    it("should resolve assets", function(done) {
      global.Image = class {
        constructor() {}
        addEventListener(e, cb) {
          cb();
        }
      };
      global.Audio = class {
        constructor(url) {
          this.url = url;
        }
        addEventListener(e, cb) {
          cb();
        }
      };
      AssetLoader.loadAssets([
        {
          name: "fakeImage",
          url: "http://fakeImage",
          type: "image"
        },
        {
          name: "fakeAudio",
          url: "http://fakeAudio",
          type: "audio"
        }
      ])
        .then(r => {
          expect(r).to.exist;
          done();
        })
        .catch(done);
    });
    it("should reject invalid types", function(done) {
      AssetLoader.loadAssets([
        {
          type: "invalid"
        }
      ])
        .then(done)
        .catch(e => {
          expect(e.message).to.eql(
            "Not all assets could be loaded: Error: Unknown type"
          );
          done();
        });
    });
  });
});
