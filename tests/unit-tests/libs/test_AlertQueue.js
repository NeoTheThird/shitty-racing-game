"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const AlertQueue = require("../../../src/libs/AlertQueue");

const expect = chai.expect;
chai.use(sinonChai);

describe("libsAlertQueue.js", function() {
  describe("constructor", function() {
    it("should construct", function() {
      expect(new AlertQueue("foo").queue).to.eql("foo");
    });
  });
  describe("alert()", function() {
    it("should create alert", function(done) {
      const fakeElement = {
        classList: {
          add: sinon.spy()
        },
        style: {}
      };
      global.document = {
        createElement: sinon.fake(type => fakeElement)
      };
      const divFake = {
        appendChild: sinon.spy(),
        removeChild: () => done()
      };
      const q = new AlertQueue(divFake);
      q.alert("everything exploded", "blue", 5);
      expect(global.document.createElement).to.have.been.calledWith("div");
      expect(divFake.appendChild).to.have.been.called;
    });
  });
});
