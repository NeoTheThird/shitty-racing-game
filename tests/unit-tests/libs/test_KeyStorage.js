"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const KeyStorage = require("../../../src/libs/KeyStorage");

const expect = chai.expect;
chai.use(sinonChai);

describe("game/KeyStorage.js", function() {
  describe("constructor()", function() {
    it("should construct keystorage", function() {
      var key = new KeyStorage();
      expect(key).to.exist;
      expect(key.storage).to.exist;
    });
  });
  describe("isDown()", function() {
    it("should return true if a key is in storage", function() {
      var key = new KeyStorage();
      key.storage["someKey"] = true;
      expect(key.isDown("someKey")).to.eql(true);
    });
    it("should return false if a key is not in storage", function() {
      var key = new KeyStorage();
      expect(key.isDown("someKey")).to.eql(false);
    });
  });
  describe("onKeydown()", function() {
    it("should store a pressed key if it has already been set", function() {
      var key = new KeyStorage();
      key.storage["someKey"] = true;
      expect(key.onKeydown("someKey")).to.have.returned;
      expect(key.storage["someKey"]).to.eql(true);
    });
    it("should store a pressed key if it hasn't already been set", function() {
      var key = new KeyStorage();
      expect(key.onKeydown("someKey")).to.have.returned;
      expect(key.storage["someKey"]).to.eql(true);
    });
  });
  describe("onKeyup()", function() {
    it("should delete a stored key", function() {
      var key = new KeyStorage();
      key.storage["someKey"] = true;
      expect(key.onKeyup("someKey")).to.have.returned;
      expect(key.storage["someKey"]).to.not.exist;
    });
    it("should do nothing if key wasn't stored", function() {
      var key = new KeyStorage();
      expect(key.onKeyup("someKey")).to.have.returned;
      expect(key.storage["someKey"]).to.not.exist;
    });
  });
  describe("Navigation functions", function() {
    [
      {
        storage: {},
        functions: [
          { f: "isSpace", r: false },
          { f: "goForwards", r: false },
          { f: "goBackwards", r: false },
          { f: "goLeft", r: false },
          { f: "goRight", r: false }
        ]
      },
      {
        storage: {
          KeyW: true
        },
        functions: [
          { f: "goForwards", r: true },
          { f: "goBackwards", r: false },
          { f: "goLeft", r: false },
          { f: "goRight", r: false }
        ]
      },
      {
        storage: {
          KeyW: true,
          ArrowUp: true
        },
        functions: [
          { f: "goForwards", r: true },
          { f: "goBackwards", r: false },
          { f: "goLeft", r: false },
          { f: "goRight", r: false }
        ]
      },
      {
        storage: {
          ArrowUp: true
        },
        functions: [
          { f: "goForwards", r: true },
          { f: "goBackwards", r: false },
          { f: "goLeft", r: false },
          { f: "goRight", r: false }
        ]
      },
      {
        storage: {
          ArrowUp: true,
          ArrowLeft: true
        },
        functions: [
          { f: "goForwards", r: true },
          { f: "goBackwards", r: false },
          { f: "goLeft", r: true },
          { f: "goRight", r: false }
        ]
      },
      {
        storage: {
          ArrowDown: true,
          ArrowRight: true
        },
        functions: [
          { f: "goForwards", r: false },
          { f: "goBackwards", r: true },
          { f: "goLeft", r: false },
          { f: "goRight", r: true }
        ]
      }
    ].forEach(variation => {
      describe("storage: " + JSON.stringify(variation.storage), function() {
        variation.functions.forEach(f => {
          it(f.f + "() should return " + f.r, function() {
            var key = new KeyStorage();
            key.storage = variation.storage;
            expect(key[f.f]()).to.eql(f.r);
          });
        });
      });
    });
  });
});
