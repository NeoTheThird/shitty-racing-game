"use strict";
const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const mocha = require("mocha");
const utils = require("../../../src/libs/utils");

const expect = chai.expect;
chai.use(sinonChai);

describe("libs/utils.js", function() {
  describe("getUrlParameters()", function() {
    it("should resolve assets", function() {
      global.window = {
        location: {
          href:
            "https://example.com/whatever.html?test=1337&something=true&answer=42&javascript=awesome"
        }
      };
      expect(utils.getUrlParameters()).to.eql({
        answer: "42",
        javascript: "awesome",
        something: "true",
        test: "1337"
      });
    });
  });
});
