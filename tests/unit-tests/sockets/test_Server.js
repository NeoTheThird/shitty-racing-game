"use strict";
const sinon = require("sinon");
const chai = require("chai");
const sinonChai = require("sinon-chai");
const proxyquire = require("proxyquire").noCallThru();
const mocha = require("mocha");
const expect = chai.expect;
chai.use(sinonChai);

const request = require("request");
const Server = require("../../../src/sockets/Server");

const _socketURL = "http://localhost:1337";

describe("sockets/Server.js", function() {
  describe("constructor()", function() {
    it("should construct server object", function() {
      const _Server = proxyquire("../../../src/sockets/Server", {});

      const server = new _Server(undefined);
      expect(server).to.exist;
      server.disconnect();
    });
  });
  describe("management functions", function() {
    describe("connect()", function() {
      it("should start server", function() {
        const server = new Server(1337);
        server.disconnect();
        server.connectSockets();
        server.disconnect();
      });
      it("should throw error if websocket already connected", function() {
        const server = new Server(1337);
        expect(sinon.spy(server.connectSockets.bind(server))).to.throw(
          "Listen method has been called more than once without closing."
        );
        server.disconnect();
      });
    });
    describe("disconnect()", function() {
      it("should disconnect", function() {
        const server = new Server(1337);
        server.disconnect();
      });
    });
  });
  describe("game functions", function() {
    describe("joinGame()", function() {
      it("should callback wait() if a player can join", function() {
        const socketIoFake = {
          connect: sinon.spy(),
          close: sinon.spy(),
          on: sinon.spy(),
          sockets: {
            emit: sinon.spy()
          }
        };
        const _Server = proxyquire("../../../src/sockets/Server", {
          "socket.io": () => socketIoFake,
          http: {
            createServer: () => {
              return {
                listen: () => {}
              };
            }
          }
        });
        const waitSpy = sinon.spy();
        const fullSpy = sinon.spy();
        const server = new _Server(1337);
        server.joinGame("a", "alice", "blue", waitSpy, fullSpy);
        expect(waitSpy).to.have.been.called.calledOnce;
        expect(fullSpy).to.not.have.been.called.called;
        server.disconnect();
      });
    });
  });
  describe("pages", function() {
    ["", "index.html", "play.html"].forEach(page => {
      it("should send /" + page, function(done) {
        const server = new Server(1337);
        request.get(_socketURL + "/" + page, function(err, res, body) {
          server.disconnect();
          done(res.statusCode !== 200);
        });
      });
    });
  });
  describe("redirects", function() {
    ["docs", "coverage", "source"].forEach(page => {
      it(
        "should send /" + page,
        function(done) {
          const server = new Server(1337);
          request.get(_socketURL + "/" + page, function(err, res, body) {
            server.disconnect();
            done(res.statusCode !== 200 && res.statusCode !== 404);
          });
        },
        5000
      );
    });
  });
});
