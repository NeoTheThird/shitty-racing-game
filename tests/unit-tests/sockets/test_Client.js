"use strict";
const sinon = require("sinon");
const chai = require("chai");
const sinonChai = require("sinon-chai");
const proxyquire = require("proxyquire").noCallThru();
const mocha = require("mocha");
const expect = chai.expect;
chai.use(sinonChai);

const _socketURL = "http://localhost:1337";

describe("sockets/Client.js", function() {
  describe("constructor()", function() {
    it("should construct client object", function() {
      const elementSpy = sinon.spy();
      global.document = {
        getElementById: sinon.fake.returns(elementSpy),
        addEventListener: sinon.spy()
      };
      global.window = {
        addEventListener: sinon.spy()
      };

      const settingsStoreSpy = sinon.spy();
      const settingsGetColorSpy = sinon.fake.returns("blue");
      const alertQueueAlertSpy = sinon.fake.returns("blue");
      const Client = proxyquire("../../../src/sockets/Client", {
        "../libs/Settings": function Settings() {
          this.store = settingsStoreSpy;
          this.getColor = settingsGetColorSpy;
        },
        "../game/ui/AlertQueue": function AlertQueue() {
          this.alert = alertQueueAlertSpy;
        },
        "socket.io-client": sinon.fake.returns({
          id: "1337",
          connect: sinon.spy(),
          on: sinon.spy()
        })
      });

      const client = new Client(undefined);
      expect(client).to.exist;
      expect(global.window.addEventListener).to.have.been.calledOnceWith(
        "beforeunload"
      );
      expect(global.document.addEventListener).to.have.been.calledWith(
        "keydown"
      );
      expect(global.document.addEventListener).to.have.been.calledWith("keyup");
      expect(global.document.getElementById).to.have.been.calledWith(
        "join_button"
      );
    }, 3000);
  });
});
