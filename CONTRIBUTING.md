# Git Guidelines

For a better consistency it is important to have basic guidelines for using git and working in Gitlab.

**All git operations (e.g. names, descriptions, commit messages, etc.) should be in English**

## Git primer

You'll want to keep your local `master` branch completely in sync with origin, so when you navigate to your local working copy, the first thing you'll do is `git checkout master` and `git pull --rebase` to fetch all the latest commits and apply them to your local tree. Now if you want to implement a new feature, you will create a branch based off the current status of master by going `git checkout -b nameofbranch` (**the branch name should be lowercase and in english**). Then you make your changes. If you want to create a commit, you `git add path/to/files` for all your files you want to stage (or `git add .` to stage everything) and then go `git commit -m "My Commit Message"`. As soon as you have all your commits created, go ahead and `git push origin nameofbranch` to push your branch to gitlab. Then go to the web interface to create the merge request.

Some other useful commands

- `git log` show all the recent commits
- `git checkout .` undo all changes to tracked files
- `git clean -xci` delete all untracked and ignored files
- `git diff` display all changes to tracked files
- `git status` show status information like staged and unstaged changes, deleted files, current branch, etc.
- `git cherry-pick [commit sha]` apply a single commit from another branch to your current branch

## Branches

Branch names should always start with a lowercase letter

The branch name should be as short as possible and shall describe briefly what will be done in this branch (e.g. `server-implementation`, `basic-classes`). **Note that you must not use spaces, but dashes (`-`).**

## Merge Requests

As soon as you create a new branch, you should also create a Merge Request on Gitlab.

The Merge Request title should be similar to the branch name (you can add a description for further details). As long as the merge request is in progress, the title should include the `WIP` prefix to indicate that a review is currently not needed.

Don't forget to check **Delete source branch when merge request is accepted** and **Squash commits when merge request is accepted**.

Once you finished your work, you can delete the `WIP` prefix and post your Merge Request URL into our Telegram channel for the code review.

## Logs

To see the debugging output, you can activate namespaces by setting the environment variable `DEBUG` in nodejs and the persistent variable `localStorage.debug` in the browser.

If you want to see all debugging output of the namespaces `server` and `client` while running the tests, you would run `DEBUG=client,server npm run test-pipeline`.

To see the output while user-testing, you would start the server with `DEBUG=server npm start`, navigate to `http://localhost:3000` in your browser, open the console and run `localStorage.debug="client";`. Reload the window for the changes to take effect. Don't forget to unset the storage item in the browser, as heavy logging is bad for performance.

You can also use the wildcard character (`*`) to see *everything*.

## Toolchain

Do **NOT** rely on the gitlab ci-runner to find your bugs! You can (and should!) always run the ci-toolchain locally before committing.

### Documentation

Document your code with [jsdoc](https://devhints.io/jsdoc). You can run `npm run jsdoc` to build the documentation to `./out/docs/`, open this in your browser.

### Lint

Run `npm run lint` to validate style and `npm run lint-fix` to enforce.

### Test

*Always* try to write tests for your new code, see the [sinon-chai cheat sheet](https://devhints.io/sinon-chai) for reference.

Run `npm run test-pipeline` to run all unit-tests. This will generate html coverage reports in `./coverage/lcov-report/index.html`, open this in your browser. If you want to run a single test, do `mocha ./path./to/test.js`.

### Code Review

Before merging a branch on the master branch, another team member **MUST** review your code and add threads for questions or comments about your code (if needed). As long as there are unresolved threads you cannot merge your branch.

To approve a merge request, click on the "thumbs up" emoji. Afterwards you can manually merge the merge request. (note that unfortunately it is possible to merge your Merge Request without approval, **but don't do this**).